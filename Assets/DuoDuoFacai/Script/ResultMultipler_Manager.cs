﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

namespace DuoDuDuoFacai {
    public class ResultMultipler_Manager : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static ResultMultipler_Manager m_Instance;
        //===== STRUCT =====
        //===== PUBLIC =====
        [Header("Gold Payout Multiplier")]
        public int m_PeanutGoldMultiplier;
        public int m_TurtleGoldMultiplier;
        public int m_DragonShipGoldMultiplier;
        public int m_PhoenixGoldMultiplier;
        //===== PRIVATES =====
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
        }

        void Start() {
            Betting_Manager.m_Instance.m_OnCalculateResult += f_ChangeMultiplier;
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_ChangeMultiplier(WinningObject p_Result) {
            Betting_Manager.m_Instance.m_WinningMultiplier = f_CalculateMultiplier(p_Result.m_PatternType);
        }

        public int f_CalculateMultiplier(e_SlotType p_Type) {
            if (p_Type == e_SlotType.PHOENIX && (int)Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType >= (int)e_LeagueType.GRAND) return m_PhoenixGoldMultiplier;
            else if (p_Type == e_SlotType.DRAGONSHIP && (int)Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType >= (int)e_LeagueType.MAJOR) return m_DragonShipGoldMultiplier;
            else if (p_Type == e_SlotType.TURTLE && (int)Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType >= (int)e_LeagueType.MINOR) return m_TurtleGoldMultiplier;
            else if (p_Type == e_SlotType.PEANUT && (int)Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType >= (int)e_LeagueType.MINI) return m_PeanutGoldMultiplier;
            else return 1;
        }
    }
}