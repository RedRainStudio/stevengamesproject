﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using DuoDuDuoFacai;

public class Betting_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Betting_Manager m_Instance;
    //===== STRUCT =====s
    //===== PUBLIC =====
    [Header("Bet Level Settings")]
    public List<BetLevel_Base> m_BetLevels ;
    public Action<BetLevel_Base> m_OnChangeBetLevel;
    public Action<BetLevel_Base> m_OnSpinButton;
    public Action<WinningObject> m_OnCalculateResult;
    public Action m_OnCalculateFinalResult;
    public Action<int> m_OnChangeBetLevelCurrentIndex;
    public double m_TotalWinning = 0;
    public double m_FreeWinning = 0;
    public int m_WinningMultiplier = 1;
    public int m_SpinCount;
    public bool m_IsMaxBet => m_CurrentBetLevelID == t_MaxBetLevel;
    public BetLevel_Base m_CurrentBetLevel {
        get => m_BetLevels[m_CurrentBetLevelID];
    }
    public int m_GetBetLevelID => m_CurrentBetLevelID;

    public bool m_Jackpot = false;
    //===== PRIVATES =====
    private int m_CurrentBetLevelID = 0;
    
    int t_MaxBetLevel;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    private void Start() {
        f_ChangeBetLevel(0);
        m_OnChangeBetLevel?.Invoke(m_CurrentBetLevel);
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ChangeBetLevel(int p_Int) {
        if (!SlotGame_Manager.m_Instance.m_IsSpinning) {
            m_CurrentBetLevelID += p_Int;

            f_GetMaxBetLevel();

            if (m_CurrentBetLevelID < 0) m_CurrentBetLevelID = t_MaxBetLevel;
            else if (m_CurrentBetLevelID > t_MaxBetLevel) m_CurrentBetLevelID = 0;
            
            m_OnChangeBetLevelCurrentIndex?.Invoke(m_CurrentBetLevelID);
            m_OnChangeBetLevel?.Invoke(m_CurrentBetLevel);
        }
    }

    public void f_GetMaxBetLevel() {
        for (int i = 0; i < m_BetLevels.Count; i++) {
            if (SlotGame_Manager.m_Instance.m_Player.m_Money < m_BetLevels[i].m_CurrencyBetAmount) {
                t_MaxBetLevel = i-1;
                if (t_MaxBetLevel < 0) t_MaxBetLevel = 0;
                return;
            }
        }
        t_MaxBetLevel = m_BetLevels.Count-1;
    }

    public void f_Spin(int p_Count) {
        if (!SlotGame_Manager.m_Instance.m_IsSpinning && SlotGame_Manager.m_Instance.m_Player.m_Money >=/* m_BetLevels[0].*/m_CurrentBetLevel.m_CurrencyBetAmount) {
            m_SpinCount = p_Count;
            
            BettingUI_Manager.m_Instance.f_ResetTextWinning();
            BettingUI_Manager.m_Instance.m_TotalWinningTextSpinning[0].SetActive(true);
            if (m_SpinCount > 0) {
                SlotGameUI_Manager.m_Instance.m_SpinQuantity.text = "" + m_SpinCount;
                SlotGameUI_Manager.m_Instance.m_Infinite.SetActive(false);
            }
            else if(m_SpinCount == -99) {
                SlotGameUI_Manager.m_Instance.m_SpinQuantity.text = "";
                SlotGameUI_Manager.m_Instance.m_Infinite.SetActive(true);
            }
            else {
                SlotGameUI_Manager.m_Instance.m_SpinQuantity.text = "";
                SlotGameUI_Manager.m_Instance.m_Infinite.SetActive(false);
            }
            f_ResetTotalWinning();
            SlotGame_Manager.m_Instance.m_Player.m_Money -= m_CurrentBetLevel.m_CurrencyBetAmount;
            m_OnSpinButton?.Invoke(m_CurrentBetLevel);
            
            SlotGame_Manager.m_Instance.f_Spin();
            SlotGame_Manager.m_Instance.f_SetBet(m_CurrentBetLevel.m_RealBet);
        }
    }

    public void f_Stop() {
        if (SlotGame_Manager.m_Instance.m_IsSpinning) {
            m_SpinCount = 0;
            SlotGame_Manager.m_Instance.f_Stop();
        }
    }

    public double f_CalculatingResult(Result p_Result) {
        m_WinningMultiplier = 1;
        if (FreeGames_Manager.m_Instance.m_IsFreeGames) {
            m_FreeWinning = m_TotalWinning;
        }
        else {
            m_FreeWinning = 0;
        }

        for (int i = 0; i < p_Result.m_WinningResult.Count; i++) {
            m_OnCalculateResult?.Invoke(p_Result.m_WinningResult[i]);
           
            m_TotalWinning += (p_Result.m_WinningResult[i].m_WinningValue*m_WinningMultiplier) * m_CurrentBetLevel.m_RealBet;
        }

        m_OnCalculateFinalResult?.Invoke();

        return m_TotalWinning;
    }

    public void f_ResetTotalWinning() {
        m_TotalWinning = 0;
    }

    public void f_SetBetMaxLevel() {
        if (!SlotGame_Manager.m_Instance.m_IsSpinning) {
            f_GetMaxBetLevel();
            m_CurrentBetLevelID = t_MaxBetLevel;
            m_OnChangeBetLevelCurrentIndex?.Invoke(m_CurrentBetLevelID);
            m_OnChangeBetLevel?.Invoke(m_CurrentBetLevel);
        }
    }
}
