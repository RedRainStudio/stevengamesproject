﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using Random = UnityEngine.Random;

[Serializable]
public class Choice
{
    public List<e_SlotType> m_Type = new List<e_SlotType>();
    public List<int> m_ID = new List<int>();
    private List<int> m_Values = new List<int>();
    public int m_ValueTotal;
    int t_TotalValue;

    public void f_Clear() {
        m_ID.Clear();
        m_Values.Clear();
        m_ValueTotal = 0;
    }

    public void f_Add(int p_Id, int p_Value, e_SlotType p_SlotType) {
        if (p_Value == 0) return;

        m_ID.Add(p_Id);
        m_Values.Add(p_Value);
        m_Type.Add(p_SlotType);
        m_ValueTotal += p_Value;
    }

    public int f_GetChoice(int p_Choice) {
        t_TotalValue = 0;
        for (int i = 0; i < m_Values.Count; i++) {
            t_TotalValue += m_Values[i];
            if (p_Choice <= t_TotalValue) {
                return m_ID[i];
            }
        }

        return -1;
    }

    public void f_RemoveChoice(e_SlotType p_Type) {
        if (m_Type.Contains(p_Type)) {
            m_ValueTotal -= m_Values[m_Type.IndexOf(p_Type)];
            m_Values.RemoveAt(m_Type.IndexOf(p_Type));
            m_ID.RemoveAt(m_Type.IndexOf(p_Type));
            m_Type.Remove(p_Type);
        }
    }

    public void f_AddChoice(int p_Id, int p_Value, e_SlotType p_Type) {
        if (!m_Type.Contains(p_Type)) {
            m_Values.Add(p_Value);
            m_ID.Add(p_Id);
            m_Type.Add(p_Type);
            m_ValueTotal += p_Value;
        }
    }

    public int f_GetChoiceSlotType(e_SlotType p_Choice) {
        for(int i = 0; i < m_Type.Count; i++) {
            if(m_Type[i] == p_Choice) {
                return m_ID[i];
            }
        }

        return -1;
    }
    
}

[Serializable]
public class UtilityManager_Manager {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== CLASS =====
    public Choice[] m_Choice = null;
    //===== PUBLIC =====
    //===== PRIVATES =====
    List<int> t_ListMoney = new List<int>();
    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public void f_CalculateBet(ref int p_Money, int p_BetAmount, List<int> p_Multiplier) {
        for(int i = 0; i < p_Multiplier.Count; i++) {
            t_ListMoney.Add(p_BetAmount * p_Multiplier[i]);
        }

        for(int i = 0; i < t_ListMoney.Count; i++) { 
            p_Money += t_ListMoney[i];
        }

    }
    

}
