﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

public class AudioManager_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static AudioManager_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public AudioSource m_OneTimeAudio;
    public AudioSource m_LoopAudio;
    public bool m_Mute;
    public AudioClip m_Dud;
    public AudioClip m_ParticleCoin;
    public AudioClip m_MegaWin;
    public AudioClip m_BigWin;
    public AudioClip m_Win;
    public AudioClip m_FreeGamesWin;
    public AudioClip m_DragonRoar;
    public bool m_IsPlaying;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        m_LoopAudio.mute = m_Mute;
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_PlayOneshot(AudioClip p_Clip)
    {
        if (!m_Mute)
        {
            m_OneTimeAudio.PlayOneShot(p_Clip);
            m_OneTimeAudio.PlayOneShot(p_Clip);
        }
      

    }

    public void f_PlayOneshot(AudioClip p_Clip, float p_Volume)
    {
        if (!m_Mute)
        {
            m_OneTimeAudio.PlayOneShot(p_Clip, p_Volume);
            m_OneTimeAudio.PlayOneShot(p_Clip);
        }
    }

    public void f_PlayLoop(AudioClip p_Clip)
    {
        if (!m_Mute)
        {
            m_LoopAudio.clip = p_Clip;
            m_LoopAudio.Play();
        }
    }

    public void f_StopLoop()
    {
        m_LoopAudio.Stop();
    }
    
    public IEnumerator<float> ie_PlayDelayLoop(AudioClip p_Clip) {
        m_IsPlaying = true;
        do {
            m_OneTimeAudio.time = 1.5f;
            m_OneTimeAudio.clip = p_Clip;
            m_OneTimeAudio.Play();
            yield return Timing.WaitForSeconds(1f);
            m_OneTimeAudio.Stop();
        } while (m_IsPlaying);
        m_OneTimeAudio.time = 3.5f;
        m_OneTimeAudio.clip = p_Clip;
        m_OneTimeAudio.Play();
        //m_OneTimeAudio.time = 3.8f;
        //m_OneTimeAudio.PlayOneShot(p_Clip);
        //m_OneTimeAudio.time = 3.8f;
    }

    public void f_StopDelayLoop() {
        m_IsPlaying = false;
    }

    public void f_TestBigWIn()
    {
        Timing.RunCoroutine(ie_PlayDelayLoop(m_ParticleCoin));
    }

   

}
