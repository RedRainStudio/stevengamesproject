﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using System;
using Enumeration;

public class ReelSlot_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Slot")]
    public Symbol_Gameobject[] m_Slots;
    public GameObject m_ReelExtendedAnimation;

    [Header("FaFaFa")]
    public GameObject m_ReelWildAnimation;

    [Header("Information")]
    public int m_ID;
    public bool m_WildFCExist; //JIKA 3 ROW KEBAWAH WILD SEMUA
    public int[] m_FinalValues;
    public float m_SpinTimer;
    public float m_ExtendedTime;

    [Header("System")]
    public bool m_IsSlowing;
    [ReadOnly] public bool m_IsExtended;
    [ReadOnly] public bool m_IsStop;
    [ReadOnly] public bool m_IsSpinning;
    [ReadOnly] public int m_StoppingValue;
    [ReadOnly] public bool m_IsActive;
    [ReadOnly] public bool m_IsTripleWild;

    [Header("Animation")]
    public Animator m_Animator;
    int m_Stopping;
    int m_Stopped;
    int m_Playing;
    //===== PRIVATES =====
    Transform m_SlotsContainer;
    float m_SpinTime;
    int m_LastSiblingIndex;
    int m_WildCount;
    int t_RandomIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start() {
        m_Stopping = Animator.StringToHash("IsStopping");
        m_Stopped = Animator.StringToHash("IsStopped");
        m_Playing = Animator.StringToHash("IsPlaying");

    }

    void Update(){
        if (!m_IsSpinning) return;
        if(m_IsActive) m_SpinTime -= Time.deltaTime;
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //=====================================================================
    //				    INITIALIZE
    //=====================================================================
    public void f_Initialize(int p_ID) {
        m_ID = p_ID;
        m_SlotsContainer = transform.GetChild(0);
        m_Slots = new Symbol_Gameobject[SlotGame_Manager.m_Instance.m_RowCount + 1];
        m_FinalValues = new int[SlotGame_Manager.m_Instance.m_RowCount];
        for (int i = 0; i < SlotGame_Manager.m_Instance.m_RowCount + 1; i++) f_Spawn(ref m_Slots[i], 0, i);
    }

    public void f_SetRandomSlot() {
        for (int i = 0; i < m_Slots.Length; i++) {
            f_Unspawn(m_Slots[i]);
            f_Spawn(ref m_Slots[i], UnityEngine.Random.Range(0, SlotGame_Manager.m_Instance.m_Slots.Count - 1), i);
        }
    }
    //=====================================================================
    //				    SPIN
    //=====================================================================
    public void f_Spin() {
        for (int i = 0; i < m_Slots.Length; i++){
            m_Slots[i].f_OnSpin();
        }

        m_IsTripleWild = false;
        m_SpinTime = m_SpinTimer;
        m_IsSpinning = true;
        m_IsStop = false;
        m_IsActive = false;
        m_Animator.SetBool(m_Playing, true);
    }

    public void f_ResetCount() { m_StoppingValue = m_FinalValues.Length - 1; }
    //=====================================================================
    //				    STOP
    //=====================================================================

    //CALLED IN ANIMATION EVENT
    public void f_Stop() {
        m_ReelExtendedAnimation.SetActive(false);
        m_IsSpinning = false;
        m_IsStop = true;
        m_Animator.SetBool(m_Stopped, false);
    }

    public void f_Reset() {
        m_IsActive = false;
        m_IsSlowing = false;
        m_IsExtended = false;
    }
    //=====================================================================
    //				    VALUES
    //=====================================================================

    //IF HAS BEEN STOPPED BY BUTTON
    public void f_SetFinalValues() {
        m_IsSpinning = false;
        for (int i = 0; i < SlotGame_Manager.m_Instance.m_RowCount; i++) {
            f_Unspawn(m_Slots[i]);
            f_Spawn(ref m_Slots[i], m_FinalValues[i], i);
        }
        
        f_Reset();
        m_ReelExtendedAnimation.SetActive(false);

        if (!m_Animator.GetBool(m_Stopped) && !m_Animator.GetBool(m_Stopping) && !m_Animator.GetBool(m_Playing))
        {
            
        }
        else
        {
            m_Animator.SetBool(m_Stopped, true);
        }

        m_Animator.SetBool(m_Playing, false);
        m_Animator.SetBool(m_Stopping, false);
    }

    public void f_SetResult(int[] p_Values) {
        m_FinalValues = p_Values;
        m_IsTripleWild = f_CheckTripleWild(m_FinalValues);
    }

    //CALLED IN ANIMATION EVENT (MENENTUKAN SLOT APA YANG KELUAR BERIKUTNYA)
    public void f_SetAsFirstSibling() {
        
        //INI HARUS DIBERHENTIKAN SAAT STOP PAKSA DI FREEGAMES
        if (SlotGame_Manager.m_Instance.m_IsSpinning && m_IsSpinning) {
            if (m_IsSlowing) {
                if (m_SpinTime < m_SpinTimer / 10) {
                    m_Animator.SetBool(m_Stopping, true);
                }
            }

            if (m_IsExtended) {
                m_ReelExtendedAnimation.SetActive(true);
                m_SpinTime += m_ExtendedTime;
                m_IsExtended = false;
            }
           
            if (m_SpinTime <= 0) {
                int.TryParse(m_SlotsContainer.GetChild(m_SlotsContainer.childCount - 1).name.Substring(m_SlotsContainer.GetChild(m_SlotsContainer.childCount - 1).name.Length - 1, 1), out m_LastSiblingIndex);
                if (m_StoppingValue >= 0 && m_LastSiblingIndex == m_StoppingValue) {
                    f_Unspawn(m_Slots[m_StoppingValue]);
                    if(m_IsTripleWild && FreeGames_Manager.m_Instance.m_ActiveFreeGames <= 0) {
                        if(m_StoppingValue == 0) f_Spawn(ref m_Slots[m_StoppingValue], m_FinalValues[m_StoppingValue]);
                        else f_Spawn(ref m_Slots[m_StoppingValue], f_GetRandomSlot(m_StoppingValue));
                    }
                    else if(m_IsTripleWild && FreeGames_Manager.m_Instance.m_ActiveFreeGames > 0){
                        f_Spawn(ref m_Slots[m_StoppingValue], m_FinalValues[m_StoppingValue]);
                    }
                    else {
                        f_Spawn(ref m_Slots[m_StoppingValue], m_FinalValues[m_StoppingValue]);
                    }
                    m_StoppingValue--;
                }

                m_SlotsContainer.GetChild(m_SlotsContainer.childCount - 1).SetAsFirstSibling();

                if (m_StoppingValue < 0) {
                    m_Animator.SetBool(m_Playing, false);
                    m_Animator.SetBool(m_Stopping, false);
                    m_Animator.SetBool(m_Stopped, true);
                }

            } else {
                int.TryParse(m_SlotsContainer.GetChild(m_SlotsContainer.childCount - 1).name.Substring(m_SlotsContainer.GetChild(m_SlotsContainer.childCount - 1).name.Length - 1, 1), out m_LastSiblingIndex);
                f_Unspawn(m_Slots[m_LastSiblingIndex]);
                f_Spawn(ref m_Slots[m_LastSiblingIndex], UnityEngine.Random.Range(0, SlotGame_Manager.m_Instance.m_Slots.Count - 1));
                m_Slots[m_LastSiblingIndex].transform.SetAsFirstSibling();
            }


        }

    }

    public bool f_CheckTripleWild(int[] p_Values) {
        m_WildCount = 0;
        for (int i = 0; i < p_Values.Length; i++) {
            if (SlotGame_Manager.m_Instance.m_Slots[p_Values[i]].m_Type == e_SlotType.WILD) {
                m_WildCount++;
            }
        }

        if (m_WildCount >= 3) {
            return true;
        }
        else {
            return false;
        }
    }

    public int f_GetRandomSlot(int p_StoppingValue) {
        t_RandomIndex = SlotGame_Manager.m_Instance.m_UtilityManager.m_Choice[(SlotGame_Manager.m_Instance.m_ActiveReel * SlotGame_Manager.m_Instance.m_RowCount) + m_StoppingValue].m_ID.Count;

        return SlotGame_Manager.m_Instance.m_UtilityManager.m_Choice[(SlotGame_Manager.m_Instance.m_ActiveReel * SlotGame_Manager.m_Instance.m_RowCount) + m_StoppingValue].m_ID[t_RandomIndex];
    }
    //=====================================================================
    //				    SPAWN AND UNSPAWN
    //=====================================================================
    public void f_Spawn(ref Symbol_Gameobject p_Object, int p_Index, int p_NamingIndex = -1) {
        p_Object = SlotPooling_Manager.m_Instance.f_SpawnObject(SlotGame_Manager.m_Instance.m_Slots[p_Index].m_Object, m_SlotsContainer);
        p_Object.m_Image.gameObject.SetActive(false);
        p_Object.m_Image.gameObject.SetActive(true);
        p_Object.name = "Symbol " + (p_NamingIndex > - 1 ? p_NamingIndex.ToString() : m_LastSiblingIndex.ToString());
        p_Object.f_Initialize(this, (p_NamingIndex > -1 ? p_NamingIndex : m_LastSiblingIndex));
    }

    public void f_Unspawn(Symbol_Gameobject p_Object) {
        p_Object.gameObject.SetActive(false);
        p_Object.transform.SetParent(SlotGame_Manager.m_Instance.m_EmptyTransform);
    }

    //=====================================================================
    //				    PROPERTIES
    //=====================================================================

    public bool m_IsCompleted => !m_IsSpinning && m_IsStop;
    
}
