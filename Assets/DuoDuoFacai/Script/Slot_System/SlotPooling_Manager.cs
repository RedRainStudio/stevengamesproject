﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ObjectPooler;
using Enumeration;

public class SlotPooling_Manager : PoolingManager_Manager<Symbol_Gameobject>
{
    public static SlotPooling_Manager m_Instance;
    public void Awake() {
        m_Instance = this;
    }

    public override bool f_AdditionalValidation(int p_Index, Symbol_Gameobject p_Object) {
        if (p_Object.m_SymbolType == m_PoolingContainer[p_Index].m_SymbolType) return true;

        return false;
    }
}
