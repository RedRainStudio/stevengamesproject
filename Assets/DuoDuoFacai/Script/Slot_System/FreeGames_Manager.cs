﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumeration;
[Serializable]
public class FreeGames_Manager : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static FreeGames_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<int> m_FreeGamesCount = new List<int>();
    public int m_ActiveFreeGames;
    public int m_TotalFreeGames;
    public bool m_IsFreeGames;

    [Header("FAFAFA")]
    public int m_IndexReel;
    public bool m_KumbangDone;
    public List<KumbangFreeGames_GameObject> m_Kumbang;
    public List<GameObject> m_WildPanelInverse;

    public bool m_IsFaFaFa;
    [Header("Five Dragon")]
    public GameObject m_FiveDragonMenu;
    public List<Animator> m_ListDragonsAnimations;
    //===== PRIVATES =====

    //=====================================================================
    //				    MONOBEHAVIOUR
    //=====================================================================
    private void Awake() {
        m_Instance = this;
    }
    private void Start() {

        
    }
    //=====================================================================
    //				    METHOD
    //=====================================================================

    //DIPANGGIL DI EVENT
    public void f_StartFreeGames() {
        m_TotalFreeGames = 0;
        Betting_Manager.m_Instance.f_ResetTotalWinning();
        Timing.RunCoroutine(co_StartFreeGames());
    }
    public void f_TestFreeGames() {
        m_TotalFreeGames = 0;
        Betting_Manager.m_Instance.f_ResetTotalWinning();
        Timing.RunCoroutine(co_TestFreeGames());
    }
    public IEnumerator<float> co_StartFreeGames() {
        for (int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
            for (int j = 0; j < SlotGame_Manager.m_Instance.m_RowCount; j++) {
                if (SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_SymbolType == e_SlotType.SCATTER) {
                    SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_Animation.Play(SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_AnimationName);
                }
            }
        }

        do {
            yield return Timing.WaitForOneFrame;

        } while (!SlotGame_Manager.m_Instance.m_ScatterAnimationDone);

        f_AddFreeGames(SlotGame_Manager.m_Instance.m_ScatterCount - 3);

        m_IsFreeGames = true;

        SlotGameUI_Manager.m_Instance.f_SetFreeGames(true);

        do {
            yield return Timing.WaitForOneFrame;
        } while (SlotGameUI_Manager.m_Instance.f_IsStartedFreeGames);

        SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(false);
        SlotGameUI_Manager.m_Instance.m_FreeGamesTemporary.SetActive(true);
        SlotGameUI_Manager.m_Instance.m_FreeGamesStart.SetActive(false);
        SlotGameUI_Manager.m_Instance.f_SetBackground(true);

        if (m_IsFaFaFa) {
            for (int i = 0; i < m_Kumbang.Count; i++) m_Kumbang[i].gameObject.SetActive(true);

            do {
                yield return Timing.WaitForOneFrame;
            } while (!m_KumbangDone);
        }
       

        SlotGame_Manager.m_Instance.f_Spin();
    }

    public IEnumerator<float> co_TestFreeGames() {
        f_AddFreeGames(SlotGame_Manager.m_Instance.m_ScatterCount - 3);

        m_IsFreeGames = true;

        SlotGameUI_Manager.m_Instance.f_SetFreeGames(true);

        do {
            yield return Timing.WaitForOneFrame;
        } while (SlotGameUI_Manager.m_Instance.f_IsStartedFreeGames);

        SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(false);
        SlotGameUI_Manager.m_Instance.m_FreeGamesTemporary.SetActive(true);
        SlotGameUI_Manager.m_Instance.m_FreeGamesStart.SetActive(false);
        SlotGameUI_Manager.m_Instance.f_SetBackground(true);

        if (m_IsFaFaFa) {
            for (int i = 0; i < m_Kumbang.Count; i++) m_Kumbang[i].gameObject.SetActive(true);

            do {
                yield return Timing.WaitForOneFrame;
            } while (!m_KumbangDone);
        }

        SlotGame_Manager.m_Instance.f_Spin();
    }

    public void f_AddFreeGames(int p_ID) {
        m_ActiveFreeGames += m_FreeGamesCount[p_ID];
        m_TotalFreeGames += m_FreeGamesCount[p_ID];
    }

    //FIVE DRAGON

    public void f_OpenDragonMenu() {
        Betting_Manager.m_Instance.f_ResetTotalWinning();
        m_FiveDragonMenu.SetActive(true);
    }

    public void f_PickDragon(int p_FreeGames) {
        m_ActiveFreeGames = p_FreeGames;
        m_TotalFreeGames = p_FreeGames;

        m_IsFreeGames = true;
        SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(false);
        SlotGameUI_Manager.m_Instance.m_FreeGamesTemporary.SetActive(true);
        SlotGame_Manager.m_Instance.f_Spin();
    }

    public void f_PickDragons(int p_ID)
    {
        m_ListDragonsAnimations[p_ID].Play("dragon");
    }


    //FIVE DRAGON START DAN DONE
    public void f_OnStartFree() {
        Timing.RunCoroutine(co_StartFreeGamesFiveDragons());
    }

    public IEnumerator<float> co_StartFreeGamesFiveDragons() {
        for (int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
            for (int j = 0; j < SlotGame_Manager.m_Instance.m_RowCount; j++) {
                if (SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_SymbolType == e_SlotType.SCATTER) {
                    SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_Animation.Play(SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_AnimationName);
                }
            }
        }

        do {
            yield return Timing.WaitForOneFrame;
        } while (!SlotGame_Manager.m_Instance.m_ScatterAnimationDone);

        f_OpenDragonMenu();
    }

    public void f_OnDoneFree() {
        SlotGameUI_Manager.m_Instance.m_FreeGamesTemporary.SetActive(false);
        SlotGame_Manager.m_Instance.f_OnFiveDragonFreeGamesDone();

    }

    /// FAFAFA
    public void f_OnFreeGamesFAFAFA(int p_ActiveReel) {
        //int m_HowManyReel = UnityEngine.Random.Range(1, 5);
        //for(int i = 0; i < m_HowManyReel; i++) {
        //if (p_ActiveReel > 0) {
        //    if (m_IndexReel == p_ActiveReel) {

               
        //        int[] t_Values = new int[SlotGame_Manager.m_Instance.m_RowCount];

        //        for (int j = 0; j < SlotGame_Manager.m_Instance.m_RowCount; j++) {
        //            t_Values[j] = 8;
        //            SlotGame_Manager.m_Instance.m_SlotResults[(p_ActiveReel * SlotGame_Manager.m_Instance.m_RowCount) + j] = 8;
        //        }
        //        SlotGame_Manager.m_Instance.m_Reels[p_ActiveReel].f_SetResult(t_Values);
        //    }
        //}

        if (SlotGame_Manager.m_Instance.m_Reels[p_ActiveReel].m_IsTripleWild) {
            m_WildPanelInverse[p_ActiveReel].SetActive(true);
        }    
    }

    public void f_OnFreeGamesDone() {
        for (int i = 0; i < m_Kumbang.Count; i++) m_Kumbang[i].gameObject.SetActive(false);
        SlotGameUI_Manager.m_Instance.f_SetBackground(false);
        m_KumbangDone = false;
    }
}
