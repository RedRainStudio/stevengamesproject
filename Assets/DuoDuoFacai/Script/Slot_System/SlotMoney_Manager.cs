﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumeration;
public class SlotMoney_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    //public static SlotMoney_Manager m_Instance;

    //===== STRUCT =====

    //===== PUBLIC =====
    public double m_JackpotWinAmount;
    //===== PRIVATES =====
    protected double t_CurrentAmountAnimation;
    protected double t_MoneyPerSecond;
    protected double m_WinningMoney;
    protected double m_BigWinningMoney;
    protected double m_FreeWinningMoney;
    protected double t_WinningMoney;
    protected double t_CurrentMoneyAnimation;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    private void Awake() {
       // m_Instance = this;
    }
    private void Start() {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public virtual IEnumerator<float> f_UpdateMoney(double p_Amount) {
  
        if(p_Amount <= 0)
        {
            AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Dud, 1f);
            yield return Timing.WaitForOneFrame;
        }
        else
        {
            if (FreeGames_Manager.m_Instance.m_ActiveFreeGames > 0) {
                Debug.Log("p_Amount : " + p_Amount);
                Debug.Log("Winning Money : " + m_WinningMoney);
                if (p_Amount - m_WinningMoney > 0)
                {
                    AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
                }
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount - m_WinningMoney, p_Result => {
                    m_WinningMoney += p_Result;
                    SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                }, 100)));

                if (SlotGame_Manager.m_Instance.m_Result.m_IS5OfAKind) yield return Timing.WaitForSeconds(2f);
                else yield return Timing.WaitForSeconds(0.5f);

                if (p_Amount - m_WinningMoney > Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount * 10) {
                    
                    f_ResetBigWinningMoney();
                    SlotGameUI_Manager.m_Instance.f_SetBigWin(p_Amount - m_WinningMoney, Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount);
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        m_BigWinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.BIGWINMONEY, m_BigWinningMoney.ToString("N0"));
                    }, 1000)));

                    SlotGameUI_Manager.m_Instance.m_BigWinCloseButton.SetActive(true);

                    do {
                        yield return Timing.WaitForOneFrame;
                    } while (SlotGameUI_Manager.m_Instance.f_IsBigWin);
                    SlotGameUI_Manager.m_Instance.m_Audio.mute = false;
                    yield return Timing.WaitForSeconds(0.5f);

                }
            }
            else {
                //HABIS FREE GAMES BERARTI
                if (m_WinningMoney > 0) 
                {
                    if (p_Amount - m_WinningMoney > 0)
                    {
                        AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
                    }
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount - m_WinningMoney, p_Result => {
                        m_WinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                    
                    }, 100)));
                    
                    if (SlotGame_Manager.m_Instance.m_Result.m_IS5OfAKind) yield return Timing.WaitForSeconds(2f);
                    else yield return Timing.WaitForSeconds(0.5f);

                    if (p_Amount - m_WinningMoney > Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount * 10) {
                        f_ResetBigWinningMoney();
                        SlotGameUI_Manager.m_Instance.f_SetBigWin(p_Amount - m_WinningMoney, Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount);
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                            m_BigWinningMoney += p_Result;
                            SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.BIGWINMONEY, m_BigWinningMoney.ToString("N0"));
                        }, 500)));

                        SlotGameUI_Manager.m_Instance.m_BigWinCloseButton.SetActive(true);
                        do {
                            yield return Timing.WaitForOneFrame;
                        } while (SlotGameUI_Manager.m_Instance.f_IsBigWin);
                        SlotGameUI_Manager.m_Instance.m_Audio.mute = false;
                    }

                    yield return Timing.WaitForSeconds(0.5f);
                    m_FreeWinningMoney = 0;
                    SlotGameUI_Manager.m_Instance.m_FreeGamesDoneCloseButton.enabled = false;
                    //FREEGAMES
                    SlotGameUI_Manager.m_Instance.f_SetFreeGames(false);
                    AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_FreeGamesWin, 1f);
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        m_FreeWinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.FREEGAMESMONEY, m_FreeWinningMoney.ToString("N0"));
                    }, 500)));

                    SlotGameUI_Manager.m_Instance.m_FreeGamesDoneCloseButton.enabled = true;

                    do {
                        yield return Timing.WaitForOneFrame;
                    } while (!SlotGameUI_Manager.m_Instance.f_IsDoneFreeGames);
                    SlotGameUI_Manager.m_Instance.m_Audio.mute = false;

                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                    }, 100)));

                    SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(true);

                }
                else
                {
                    AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                        m_WinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                    }, 100)));

                    if (SlotGame_Manager.m_Instance.m_Result.m_IS5OfAKind) yield return Timing.WaitForSeconds(2f);
                    else yield return Timing.WaitForSeconds(0.5f);

                    if (p_Amount > Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount * 10) {
                        f_ResetBigWinningMoney();
                        SlotGameUI_Manager.m_Instance.f_SetBigWin(p_Amount, Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount);
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                            m_BigWinningMoney += p_Result;
                            SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.BIGWINMONEY, m_BigWinningMoney.ToString("N0"));
                        }, 500)));
                     
                        SlotGameUI_Manager.m_Instance.m_Audio.mute = false;
                        SlotGameUI_Manager.m_Instance.m_BigWinCloseButton.SetActive(true);
                    }

                }

                m_WinningMoney = 0;
            }
        }
        AudioManager_Manager.m_Instance.f_StopDelayLoop();

    }

    public IEnumerator<float> f_UpdateText(double p_Amount, Action<double> p_Callback, double p_MoneyPerSecond) {
        t_CurrentAmountAnimation = 0;
        t_MoneyPerSecond = p_Amount / p_MoneyPerSecond;
        for (int i = 0; i < p_MoneyPerSecond; i++) {
            p_Callback(t_MoneyPerSecond);
            t_CurrentAmountAnimation += t_MoneyPerSecond;
            yield return Timing.WaitForOneFrame;
        }
        //while (t_CurrentAmountAnimation != p_Amount) {
           
        //};

    }

    public void f_ResetWinningMoney() {
        m_WinningMoney = 0;
        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
    }
    public void f_ResetBigWinningMoney() {
        m_BigWinningMoney = 0;
        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.BIGWINMONEY, m_WinningMoney.ToString("N0"));
    }

    public virtual IEnumerator<float> f_UpdateJackPot() {
        do {
            yield return Timing.WaitForOneFrame;
        } while (Betting_Manager.m_Instance.m_Jackpot);

        if (m_JackpotWinAmount > 0) {
            if (FreeGames_Manager.m_Instance.m_ActiveFreeGames > 0) {
                t_CurrentMoneyAnimation = 0;
                t_MoneyPerSecond = m_JackpotWinAmount / 100;
                while (t_CurrentMoneyAnimation != m_JackpotWinAmount) {
                    t_WinningMoney += t_MoneyPerSecond;
                    t_CurrentMoneyAnimation += t_MoneyPerSecond;

                    yield return Timing.WaitForOneFrame;
                };
            }
            else {
                t_CurrentMoneyAnimation = 0;
                t_MoneyPerSecond = m_JackpotWinAmount / 100;
                while (t_CurrentMoneyAnimation != m_JackpotWinAmount) {
                    t_CurrentMoneyAnimation += t_MoneyPerSecond;
                    SlotGame_Manager.m_Instance.m_Player.m_Money += t_MoneyPerSecond;

                    yield return Timing.WaitForOneFrame;
                };
            }
        }

        m_JackpotWinAmount = 0;
    }
}
