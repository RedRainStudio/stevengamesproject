﻿using Enumeration;
using MEC;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using UnityEngine;
using Random = UnityEngine.Random;
using Debug = UnityEngine.Debug;

public class SlotGame_Manager : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES
    //=====================================================================
    //===== SINGLETON =====
    public static SlotGame_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("Reels")]
    public List<ReelSlot_GameObject> m_Reels = new List<ReelSlot_GameObject>();
    public bool m_IsExtended;


    [Header("Slots")]
    public List<Slot> m_Slots = new List<Slot>();

    [Header("PayLines")]
    public List<PayLines> m_PayLines = new List<PayLines>();

    [Header("Markers")]
    public List<Marker> m_Markers = new List<Marker>();

    [Header("Result")]
    public Result m_Result;
    public List<WinningPattern> m_WinningPatterns = new List<WinningPattern>();

    [Header("Deck")]
    public List<int> m_SlotResults = new List<int>();

    [Header("Utility")]
    public UtilityManager_Manager m_UtilityManager = new UtilityManager_Manager();

    [Header("Bets")]
    public float m_ActiveBet;

    [Header("Player")]
    public Player_Properties m_Player = new Player_Properties();

    [Header("Money Manager")]
    public SlotMoney_Manager m_MoneyManager;

    [Header("Game System")]
    public int m_RowCount;
    public int m_ReelCount;
    [ReadOnly] public int m_ScatterCount;
    [ReadOnly] public int m_WildCount;
    [ReadOnly] public int m_WildMatchCount;
    [ReadOnly] public bool m_IsSpinning;
    [ReadOnly] public int m_ActiveReel;
    [ReadOnly] public float m_SpinTime;
    [ReadOnly] public bool m_SymbolAnimationDone;
    [ReadOnly] public bool m_ScatterAnimationDone;

    [Header("Event")]
    public TransformEvent m_OnWildFound;
    public Event m_OnWild;
    public Event m_OnSpinDone;
    public Event m_OnFreeGamesStart;
    public IntEvent m_OnFreeGames;
    public Event m_OnFreeGamesDone;
    public IntEvent m_OnReelSpinStart;
    public IntEvent m_OnReelSpinDone;

    [Header("Other")]
    public Transform m_EmptyTransform;
    public TextAsset m_Texts;
    public bool m_CanSpin; 
    public bool m_DoneFinalCheck;
    [ReadOnly] public string[] m_Parser1;
    [ReadOnly] public string[] m_Parser2;
   
    //===== PRIVATES =====
    List<int> t_SlotResults = new List<int>();
    int t_RandomValue;
    int t_SymbolIndex;
    bool m_StartFromBack;
    int[] t_Value;
    bool t_IsScatter;
    bool t_IsWild;
    bool t_IsAngpao;
    bool t_IsDefectWild;
    float m_HoldThreshold;
    bool m_Threshold;
    float m_WaitTime;
    bool m_AfterSpin;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }
    public void f_Test(int i) {
        Debug.Log(i);
    }
    void Start() {
        Application.targetFrameRate = 60;
        for (int i = 0; i < m_Reels.Count; i++) {
            m_Reels[i].f_Initialize(i);
        }
        f_CreateChoice();
        f_SetRandomSlot();
        f_Reset();
        for (int i = 0; i < m_UtilityManager.m_Choice.Length; i++) {
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.ANGPAO);
        }
        //f_CreateMarker();
        //f_SetPayLine(0);
        //f_SetBet(0);
    }

    void Update(){
        f_DelayButton(0.2f);
        if (m_Threshold) m_HoldThreshold += Time.deltaTime;
        if(m_HoldThreshold > .5f && !SlotGameUI_Manager.m_Instance.m_SpinHold.activeSelf) SlotGameUI_Manager.m_Instance.m_SpinHold.SetActive(true);
        //FOR DEBUG PURPOSES
        if (!m_IsSpinning) return;

        m_SpinTime += Time.deltaTime;

        if (m_ActiveReel == -1) {
            if(m_SpinTime > 0f) {
                m_ActiveReel = 0;
                m_Reels[m_ActiveReel].m_IsActive = true;
                m_Reels[m_ActiveReel].f_ResetCount();
                f_SetResult();
            }
        }
        else {
            if (m_Reels[m_ActiveReel].m_IsCompleted) {
                m_Reels[m_ActiveReel].f_Reset();
                f_CheckScatter(m_Reels[m_ActiveReel].m_FinalValues);
                m_ActiveReel++;
                if (m_ActiveReel < m_Reels.Count) {
                    if(m_ScatterCount >= 2) f_OnScatterCountMoreThan2(ref m_Reels[m_ActiveReel].m_IsExtended, ref m_Reels[m_ActiveReel].m_IsSlowing);
                    m_Reels[m_ActiveReel].m_IsActive = true;
                }

                if (m_ActiveReel < m_Reels.Count) m_Reels[m_ActiveReel].f_ResetCount();
            }

            if (m_ActiveReel >= m_Reels.Count || !m_IsSpinning) {
                m_IsSpinning = false;
                Timing.RunCoroutine(f_CheckWin());
            }
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //=====================================================================
    //				   READ PAYLINES FROM CSV
    //=====================================================================
    public void f_ReadCSVFile() {
        //m_Texts = Resources.Load<TextAsset>("PayLinesData");
        string fs = m_Texts.text;
        m_Parser1 = Regex.Split(fs, " \n| \r|\r\n");

        for(int i = 0; i < m_Parser1.Length; i++) {
            string valueLine = m_Parser1[i];
            m_Parser2 = Regex.Split(valueLine, ";");
            m_PayLines.Add(f_GetPayLines(m_Parser2));
        }
    }

    public PayLines f_GetPayLines(string[] p_PayLinesNumber) {
        return new PayLines(p_PayLinesNumber);
    }

    //=====================================================================
    //				    INITIALIZATION
    //=====================================================================
    //=====================================================================
    //				   SCATTER & WILD CHECK
    //=====================================================================

   
    public void f_CheckScatter(int[] p_Values) {
        for(int i = 0; i < p_Values.Length; i++) {
            if(m_Slots[p_Values[i]].m_Type == e_SlotType.SCATTER) {
                m_ScatterCount++;
            }
        }
    }

    public void f_CheckWild() {
        m_WildCount = 0;
        for(int i = 0; i < m_Reels.Count; i++) {
            for(int j = 0;j< m_Reels[i].m_Slots.Length - 1; j++) {
                if(m_Reels[i].m_Slots[j].m_SymbolType == e_SlotType.WILD) {
                    int m_StateID = Animator.StringToHash(m_Reels[i].m_Slots[j].m_AnimationName + "2");
                    if (m_Reels[i].m_Slots[j].m_Animation.HasState(0, m_StateID)) {
                        m_Reels[i].m_Slots[j].m_Animation.Play(m_Reels[i].m_Slots[j].m_AnimationName + "2");
                    }
                    m_OnWildFound?.Invoke(m_Reels[i].m_Slots[j].transform);
                    m_WildCount++;
                }
            }        
        }
    }

    public IEnumerator<float> f_FinalCheck() {
        m_DoneFinalCheck = true;
        m_OnWild?.Invoke();

        do {
            Debug.Log(m_DoneFinalCheck);
            yield return Timing.WaitForOneFrame;
        } while (!m_DoneFinalCheck);
       
    }
    //=====================================================================
    //	    WHAT HAPPENED TO SPIN IF SCATTER MORE THAN 2
    //=====================================================================
    public void f_OnScatterCountMoreThan2(ref bool p_IsExtended, ref bool p_IsSlowing) {
        p_IsExtended = m_IsExtended;
        p_IsSlowing = false;
    }

    //=====================================================================
    //				   CREATE A RANDOM CHOICE FOR SLOT
    //=====================================================================
    public void f_CreateChoice() {
        m_UtilityManager.m_Choice = new Choice[m_Reels.Count * m_RowCount];
        for(int i = 0; i < m_Reels.Count * m_RowCount; i++) {
            m_UtilityManager.m_Choice[i] = new Choice();
            m_UtilityManager.m_Choice[i].f_Clear();

            for(int j = 0; j < m_Slots.Count; j++) {
                if (f_GetSlot(j).m_Frequency[i] != 0) m_UtilityManager.m_Choice[i].f_Add(j, f_GetSlot(j).m_Frequency[i], f_GetSlot(j).m_Type);
            }
        }
    }

    #region belom tentu dipake
    public void f_CreateMarker() {
        //TODO : CREATE A MARKER IN THE START OF THE GAME
    }

    public void f_SetPayLine(int p_Line) {
        //TODO : CREATE MARKER PAYLINES AND SET BET
    }
    #endregion

    //=====================================================================
    //				   CREATE A RANDOM SLOT FOR VISUAL PURPOSES
    //=====================================================================
    public void f_SetRandomSlot() {
        for(int i = 0; i < m_Reels.Count; i++) {
            m_Reels[i].f_SetRandomSlot();
        }
    }

    //=====================================================================
    //				   STOP
    //=====================================================================

    //CALLED IN BUTTON
    public void f_Stop() {
        m_ActiveReel = m_Reels.Count - 1;
        m_ScatterCount = 0;
        m_IsSpinning = false;
        f_CheckScatter(m_SlotResults.ToArray());
        for (int i = 0; i < m_Reels.Count; i++) m_Reels[i].f_SetFinalValues();
        Timing.RunCoroutine(f_CheckWin());
       
    }

    //=====================================================================
    //				    SPIN
    //=====================================================================
    public void f_OnSpinButtonDown() {
        m_Threshold = true;
    }

    public void f_OnSpinButtonUp() {
        m_Threshold = false;
        if(m_HoldThreshold > .5f) {
            SlotGameUI_Manager.m_Instance.m_SpinHold.SetActive(true);
        }
        else {
            Betting_Manager.m_Instance.f_Spin(0);
        }
        m_HoldThreshold = 0;
    }

    //public IEnumerator<float> ie_DelayButton(float p_WaitTime) {
    //    yield return Timing.WaitForSeconds(p_WaitTime);
    //    SlotGameUI_Manager.m_Instance.f_SetSpinButton(2);
    //}

    public void f_DelayButton(float p_WaitTime) {
        if (m_AfterSpin) {
            SlotGameUI_Manager.m_Instance.f_SetSpinButton(1);
            m_WaitTime += 0.02f;
            if (m_WaitTime >= p_WaitTime) {
                m_WaitTime = 0;
                SlotGameUI_Manager.m_Instance.f_SetSpinButton(2);
                m_AfterSpin = false;
            }
        }
        
    }
    //CALLED IN BUTTON
    public void f_Spin() {
        //Timing.RunCoroutine(ie_DelayButton(0.1f));
        m_AfterSpin = true;
        m_SpinTime = 0.0f;
        m_ActiveReel = -1;
        m_IsSpinning = true;
        m_ScatterCount = 0;
        m_WildMatchCount = 0; 
        BettingUI_Manager.m_Instance.m_DoneSpinning = false;
        if (f_GetActiveFreeGames() <= 0) {
            m_MoneyManager.f_ResetWinningMoney();
        }
        else {
            if (FiveDragon.ResultMultipler_Manager.m_Instance) {
                FiveDragon.ResultMultipler_Manager.m_Instance.m_CurrentMultiplier = FiveDragon.ResultMultipler_Manager.m_Instance.f_CalculateMultiplier();
                switch (FiveDragon.ResultMultipler_Manager.m_Instance.m_CurrentMultiplier)
                {
                    case 2: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[0]; break;
                    case 3: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[1]; break;
                    case 5: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[2]; break;
                    case 8: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[3]; break;
                    case 10: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[4]; break;
                    case 15: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[5]; break;
                    case 30: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[6]; break;
                }
                SlotGameUI_Manager.m_Instance.m_MultiplierFiveDragons.text = "x" + FiveDragon.ResultMultipler_Manager.m_Instance.m_CurrentMultiplier;
            }

            FreeGames_Manager.m_Instance.m_IndexReel = UnityEngine.Random.Range(0, 4);
            FreeGames_Manager.m_Instance.m_ActiveFreeGames--;
        }
        f_Reset();
        m_SymbolAnimationDone = false;
        m_ScatterAnimationDone = false;
        if(SlotGameUI_Manager.m_Instance.m_FiveOfAKind != null) SlotGameUI_Manager.m_Instance.m_FiveOfAKind.SetActive(false);
        for (int i = 0; i < m_Reels.Count; i++) {
            for (int j = 0; j < m_Reels[i].m_Slots.Length; j++) {
                m_Reels[i].m_Slots[j].m_Animation.Play("Idle");
                m_Reels[i].m_Slots[j].m_Highlight.SetActive(false);
                m_Reels[i].m_Slots[j].transform.GetChild(1).gameObject.SetActive(true);
            }
            m_Reels[i].f_Spin();
        }
    }

    public void f_FreeGames() {
        m_MoneyManager.f_ResetWinningMoney();
        SlotGameUI_Manager.m_Instance.f_SetSpinButton(1);
        //FreeGames_Manager.m_Instance.m_IsFreeGames = true;
        //FreeGames_Manager.m_Instance.m_ActiveFreeGames = 10;
        m_ScatterCount = 3;
        f_RemoveCardsChance();
        FreeGames_Manager.m_Instance.f_TestFreeGames();
        //Timing.RunCoroutine(f_CheckFreeGames());
    }

    public void f_TestFAFAFAFreeGames() {
        m_MoneyManager.f_ResetWinningMoney();
        SlotGameUI_Manager.m_Instance.f_SetSpinButton(1);
        //FreeGames_Manager.m_Instance.m_IsFreeGames = true;
        //FreeGames_Manager.m_Instance.m_ActiveFreeGames = 10;
        m_ScatterCount = 3;
        FreeGames_Manager.m_Instance.f_TestFreeGames();
        //Timing.RunCoroutine(f_CheckFreeGames());
    }


    public void f_ChangeScene(string p_Scene) {
        UnityEngine.SceneManagement.SceneManager.LoadScene(p_Scene);
    }
    //=====================================================================
    //				    RESULT
    //=====================================================================

    public void f_SetResult() {
        m_SlotResults = f_CheckResult();

        for(int i = 0; i < m_Reels.Count; i++) {
            t_Value = new int[m_RowCount];

            for(int j = 0; j < m_RowCount; j++) {
                t_Value[j] = m_SlotResults[m_RowCount * i + j];
            }
            m_Reels[i].f_SetResult(t_Value);
            if(FreeGames_Manager.m_Instance.m_IsFreeGames)m_OnFreeGames?.Invoke(i);
        }

    }

    public List<int> f_CheckResult() {
        t_SlotResults.Clear();

        for (int i = 0; i < m_Reels.Count; i++) {
            t_IsScatter = false;
            t_IsWild = false;
            t_IsAngpao = false;
            t_IsDefectWild = false;
            for (int j = 0; j < m_RowCount; j++) {
                m_OnReelSpinStart?.Invoke((m_RowCount * i) + j);
                t_RandomValue = -1;
                t_RandomValue = Random.Range(0, m_UtilityManager.m_Choice[(m_RowCount * i) + j].m_ValueTotal);
                t_RandomValue = Random.Range(0, m_UtilityManager.m_Choice[(m_RowCount * i) + j].m_Type.Count - 1);

                //t_SymbolIndex = m_UtilityManager.m_Choice[(m_RowCount * i) + j].f_GetChoice(t_RandomValue);
                //if ((m_RowCount * i) + j == 0) t_RandomValue = 24;
                //if ((m_RowCount * i) + j == 1) t_RandomValue = 22;
                //if ((m_RowCount * i) + j == 2) t_RandomValue = 20;
                //if ((m_RowCount * i) + j == 3) t_RandomValue = 24;
                //if ((m_RowCount * i) + j == 4) t_RandomValue = 10;
                //if ((m_RowCount * i) + j == 5) t_RandomValue = 24;
                //if ((m_RowCount * i) + j == 6) t_RandomValue = 22;
                //if ((m_RowCount * i) + j == 7) t_RandomValue = 21;
                //if ((m_RowCount * i) + j == 8) t_RandomValue = 18;
                //if ((m_RowCount * i) + j == 9) t_RandomValue = 1;
                //if ((m_RowCount * i) + j == 10) t_RandomValue = 1;
                //if ((m_RowCount * i) + j == 11) t_RandomValue = 1;
                //if ((m_RowCount * i) + j == 12) t_RandomValue = 18;
                //if ((m_RowCount * i) + j == 13) t_RandomValue = 19;
                //if ((m_RowCount * i) + j == 14) t_RandomValue = 23;


                t_SymbolIndex = m_UtilityManager.m_Choice[(m_RowCount * i) + j].f_GetChoiceSlotType(m_UtilityManager.m_Choice[(m_RowCount * i) + j].m_Type[t_RandomValue]);
               

                //if (m_Slots[t_SymbolIndex].m_Type == e_SlotType.SCATTER) {
                //    t_IsScatter = true;
                //}

                //if (m_Slots[t_SymbolIndex].m_Type == e_SlotType.WILD) {
                //    t_IsWild = true;
                //}
                //if (m_Slots[t_SymbolIndex].m_Type == e_SlotType.DEFECTWILD) {
                //    t_IsDefectWild = true;
                //}
                //if (m_Slots[t_SymbolIndex].m_Type == e_SlotType.ANGPAO) {
                //    t_IsAngpao = true;
                //}


                t_SlotResults.Add(t_SymbolIndex);
                m_OnReelSpinDone?.Invoke((m_RowCount * i) + j);
            }
        }
        return t_SlotResults;
    }

    public void f_RemoveScatterAndWild(int p_Index) {
        if (t_IsScatter) {
            m_UtilityManager.m_Choice[p_Index].f_RemoveChoice(e_SlotType.SCATTER);
        }

        if (t_IsWild) {
            m_UtilityManager.m_Choice[p_Index].f_RemoveChoice(e_SlotType.WILD);
          
        }

        if (t_IsDefectWild) {
            m_UtilityManager.m_Choice[p_Index].f_RemoveChoice(e_SlotType.DEFECTWILD);
        }

        if (t_IsAngpao) {
            m_UtilityManager.m_Choice[p_Index].f_RemoveChoice(e_SlotType.ANGPAO);
        }

    }
    public void f_RemoveCardsChance() {
        for (int i = 0; i < m_UtilityManager.m_Choice.Length; i++) {
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.ACE);
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.NINE);
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.TEN);
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.JACK);
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.QUEEN);
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.KING);
        }
    }

    public void f_OnFiveDragonFreeGamesStart() {
        for (int i = 0; i < m_UtilityManager.m_Choice.Length; i++) {
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.SCATTER);
        }

        for (int i = 0; i < m_Slots.Count; i++) {
            if (m_Slots[i].m_Type == e_SlotType.ANGPAO) {
                for (int j = 0; j < m_UtilityManager.m_Choice.Length; j++) {
                    m_UtilityManager.m_Choice[j].f_AddChoice(i, f_GetSlot(i).m_Frequency[j], f_GetSlot(i).m_Type);
                }
            }
        }
    }

    public void f_OnFiveDragonFreeGamesDone() {
        for (int i = 0; i < m_UtilityManager.m_Choice.Length; i++) {
            m_UtilityManager.m_Choice[i].f_RemoveChoice(e_SlotType.ANGPAO);
        }

        for (int i = 0; i < m_Slots.Count; i++) {
            if (m_Slots[i].m_Type == e_SlotType.SCATTER) {
                for (int j = 0; j < m_UtilityManager.m_Choice.Length; j++) {
                    m_UtilityManager.m_Choice[j].f_AddChoice(i, f_GetSlot(i).m_Frequency[j], f_GetSlot(i).m_Type);
                }
            }
        }
    }

    public void f_AddCardsChance() {
        for (int i = 0; i < m_Slots.Count; i++) {
            if (m_Slots[i].m_Type == e_SlotType.NINE || m_Slots[i].m_Type == e_SlotType.TEN || m_Slots[i].m_Type == e_SlotType.QUEEN || m_Slots[i].m_Type == e_SlotType.KING || m_Slots[i].m_Type == e_SlotType.ACE || m_Slots[i].m_Type == e_SlotType.JACK) {
                for (int j = 0; j < m_UtilityManager.m_Choice.Length; j++) {
                    m_UtilityManager.m_Choice[j].f_AddChoice(i, f_GetSlot(i).m_Frequency[j], f_GetSlot(i).m_Type);
                }
            }
        }

    }

    public void f_AddScatterAndWild(int p_Index) {
        if (t_IsScatter) {
            for (int k = 0; k < m_Slots.Count; k++) {
                if (m_Slots[k].m_Type == e_SlotType.SCATTER) {
                    m_UtilityManager.m_Choice[p_Index].f_AddChoice(k, f_GetSlot(k).m_Frequency[p_Index], e_SlotType.SCATTER);
                    break;
                }
            }
        }

        if (t_IsWild) {
            for (int k = 0; k < m_Slots.Count; k++) {
                
                if (m_Slots[k].m_Type == e_SlotType.WILD) m_UtilityManager.m_Choice[p_Index].f_AddChoice(k, f_GetSlot(k).m_Frequency[p_Index], e_SlotType.WILD);
            }
        }

        if (t_IsDefectWild) {
            for (int k = 0; k < m_Slots.Count; k++) {
              
                if (m_Slots[k].m_Type == e_SlotType.DEFECTWILD) m_UtilityManager.m_Choice[p_Index].f_AddChoice(k, f_GetSlot(k).m_Frequency[p_Index], e_SlotType.DEFECTWILD);
            }
        }

        if (t_IsAngpao) {
            for (int k = 0; k < m_Slots.Count; k++) {
                if (m_Slots[k].m_Type == e_SlotType.ANGPAO) { 
                    m_UtilityManager.m_Choice[p_Index].f_AddChoice(k, f_GetSlot(k).m_Frequency[p_Index], e_SlotType.ANGPAO);
                    break;
                }
            }
        }

    }

    //=====================================================================
    //				    WINNING CONDITION
    //=====================================================================

    public IEnumerator<float> f_CheckWin() {
        SlotGameUI_Manager.m_Instance.f_SetSpinButton(1);
        f_CheckWild();
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_FinalCheck()));
        
        m_Result = f_GetResult(m_Result, m_SlotResults);
        m_Result.f_Check();
    }

    public void f_Reset() {
        m_Result.f_Reset();
    }

    public Result f_GetResult(Result p_Result, List<int> p_SlotResults) {
        //TODO : SIAPIN EVENT BUAT ATURAN TAMBAHAN
        f_Reset();
        int[] t_Result = new int[m_Reels.Count];
        int[] t_Row = new int[m_Reels.Count];
        for (int i = 0; i < m_PayLines.Count; i++) {
            PayLines m_Line = m_PayLines[i];
            string m_Result = "";
            for (int j = 0; j < m_Reels.Count; j++) {
                t_Result[j] = p_SlotResults[m_RowCount * j + m_Line.m_SlotsNumber[j]];
                t_Row[j] = m_Line.m_SlotsNumber[j];
                m_Result = m_Result + m_Line.m_SlotsNumber[j].ToString() + j.ToString();
            }

            //CHECK HAIL
            for (int k = 0; k < m_WinningPatterns.Count; k++) {
                if (f_IsMatch(m_WinningPatterns[k], t_Result)) {
                    if (m_WinningPatterns[k].m_TotalWinningReel == 3) {
                        p_Result.m_WinningResult3.Add(new WinningObject(m_WinningPatterns[k].m_Multiplier, t_Result, t_Row, m_WinningPatterns[k].m_PatternType, m_WinningPatterns[k].m_TotalWinningReel, m_Result, m_StartFromBack));
                    }
                    else if (m_WinningPatterns[k].m_TotalWinningReel == 4) {
                        p_Result.m_WinningResult4.Add(new WinningObject(m_WinningPatterns[k].m_Multiplier, t_Result, t_Row, m_WinningPatterns[k].m_PatternType, m_WinningPatterns[k].m_TotalWinningReel, m_Result,m_StartFromBack));
                    }
                    else if (m_WinningPatterns[k].m_TotalWinningReel == 5) {
                        p_Result.m_WinningResult5.Add(new WinningObject(m_WinningPatterns[k].m_Multiplier, t_Result, t_Row, m_WinningPatterns[k].m_PatternType, m_WinningPatterns[k].m_TotalWinningReel, m_Result, m_StartFromBack));
                    }
                }
            }
        }
        return p_Result;
    }

    public bool f_IsMatch(WinningPattern p_WinningCondition, int[] p_ResultIndex) {
        m_WildMatchCount = 0;
        if (f_IsAllWild(p_WinningCondition, p_ResultIndex)) {
            if(p_WinningCondition.m_PatternType == e_SlotType.WILD) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            for (int i = 0; i < p_ResultIndex.Length; i++) {
                if (i == 0 && (m_Slots[p_ResultIndex[i]].m_Type == e_SlotType.WILD || m_Slots[p_ResultIndex[i]].m_Type == e_SlotType.DEFECTWILD)) m_WildMatchCount++;
                if (i > 0) {
                    if ((m_Slots[p_ResultIndex[i]].m_Type == e_SlotType.WILD || m_Slots[p_ResultIndex[i]].m_Type == e_SlotType.DEFECTWILD)
                        && (m_Slots[p_ResultIndex[i - 1]].m_Type == e_SlotType.WILD || m_Slots[p_ResultIndex[i - 1]].m_Type == e_SlotType.DEFECTWILD)
                        && (m_Slots[p_ResultIndex[0]].m_Type == e_SlotType.WILD || m_Slots[p_ResultIndex[0]].m_Type == e_SlotType.DEFECTWILD)) {
                        m_WildMatchCount++;
                    }
                }

                //if(m_WildMatchCount == 5) {
                //    return false;
                //}else
                if (m_WildMatchCount >= 3) {
                    if (m_Slots[p_ResultIndex[i]].m_Type != e_SlotType.WILD
                        && m_Slots[p_ResultIndex[i]].m_Type != e_SlotType.DEFECTWILD
                        && m_Slots[p_ResultIndex[i]].m_Type != p_WinningCondition.m_PatternType) {
                        return false;
                    }
                }
                else {
                    if (p_WinningCondition.m_WinningPattern[i] != e_SlotType.RANDOM
                    && m_Slots[p_ResultIndex[i]].m_Type != e_SlotType.WILD
                    && m_Slots[p_ResultIndex[i]].m_Type != e_SlotType.DEFECTWILD
                    && m_Slots[p_ResultIndex[i]].m_Type != p_WinningCondition.m_WinningPattern[i]) {
                        return false;
                    }
                }
            }

            m_StartFromBack = p_WinningCondition.m_StartFromBack;
            return true;
        }
       
    }

    public bool f_IsAllWild(WinningPattern p_WinningCondition, int[] p_ResultIndex) {
        for (int i = 0; i < p_ResultIndex.Length; i++) {
            if (m_Slots[p_ResultIndex[i]].m_Type != e_SlotType.WILD && m_Slots[p_ResultIndex[i]].m_Type != e_SlotType.DEFECTWILD) return false;
        }

        return true;
    }

    //=====================================================================
    //				    FREE GAMES
    //=====================================================================
    public IEnumerator<float> f_CheckFreeGames() {
        if(FreeGames_Manager.m_Instance.m_ActiveFreeGames > 0) {
            if (m_ScatterCount >= 3) {
                m_OnFreeGamesStart?.Invoke();
            }
            else {
                yield return Timing.WaitForSeconds(.5f);
                f_Spin();
            }
        }else if(FreeGames_Manager.m_Instance.m_ActiveFreeGames <= 0) {
            if (m_ScatterCount >= 3)
            {
                yield return Timing.WaitForSeconds(.5f);
                Betting_Manager.m_Instance.m_SpinCount = 0;
                SlotGameUI_Manager.m_Instance.m_SpinQuantity.text = "";
                SlotGameUI_Manager.m_Instance.m_Infinite.SetActive(false);
                Betting_Manager.m_Instance.f_ResetTotalWinning();
                m_OnFreeGamesStart?.Invoke();
                yield break;
            }

            if (FreeGames_Manager.m_Instance.m_IsFreeGames) {
                m_OnFreeGamesDone?.Invoke();
                FreeGames_Manager.m_Instance.m_IsFreeGames = false;
            }
           
            if(Betting_Manager.m_Instance.m_SpinCount > 0) {
                yield return Timing.WaitForSeconds(.5f);
                Betting_Manager.m_Instance.f_Spin(--Betting_Manager.m_Instance.m_SpinCount);
                yield break;
            }

            if(Betting_Manager.m_Instance.m_SpinCount == -99) {
                yield return Timing.WaitForSeconds(.5f);
                Betting_Manager.m_Instance.f_Spin(-99);
                yield break;
            }

            SlotGameUI_Manager.m_Instance.f_SetSpinButton(0);
            BettingUI_Manager.m_Instance.m_DoneSpinning = true;
            BettingUI_Manager.m_Instance.t_WaitTimer = 0f;
        }
    }

    //=====================================================================
    //				    UPDATE PostGame
    //=====================================================================
    public IEnumerator<float> f_PostGame(double p_Amount) {

        BettingUI_Manager.m_Instance.f_ResetTextWinning();

        if (p_Amount > 0) {
            BettingUI_Manager.m_Instance.m_TotalWinningTextObject.SetActive(true);
        }
        else {
            BettingUI_Manager.m_Instance.m_TotalWinningTextLose.SetActive(true);
        }

        yield return Timing.WaitUntilDone(Timing.RunCoroutine(m_MoneyManager.f_UpdateMoney(p_Amount)));
       
        yield return Timing.WaitForSeconds(0.5f);

        m_OnSpinDone?.Invoke();

        yield return Timing.WaitUntilDone(Timing.RunCoroutine(m_MoneyManager.f_UpdateJackPot()));


        GC.Collect();

        Timing.RunCoroutine(f_CheckFreeGames());
        if (p_Amount > 0) {
            do {
                yield return Timing.WaitForOneFrame;
            } while (!m_SymbolAnimationDone);
            yield return Timing.WaitForSeconds(0.1f);
        }
    }
    
    ////=====================================================================
    //				    PROPERTIES
    //=====================================================================
    public Slot f_GetSlot(int p_Index) => m_Slots[p_Index];
    public float f_GetBet() => m_ActiveBet;
    public void f_SetBet(double p_BetAmount) => m_ActiveBet = (float) p_BetAmount;
    public void f_ClearPayLines() => m_PayLines.Clear();
    public void f_AddSlots(int p_ID = -1) => m_Slots.Insert((p_ID > -1) ? p_ID : m_Slots.Count, new Slot());
    public void f_RemoveSlots(int p_ID = -1) => m_Slots.RemoveAt((p_ID > -1) ? p_ID : m_Slots.Count - 1);
    public void f_InsertWinningPattern(int p_ID = -1) => m_WinningPatterns.Insert((p_ID > -1) ? p_ID : m_WinningPatterns.Count, new WinningPattern());
    public void f_RemoveWinningPattern(int p_ID = -1) => m_WinningPatterns.RemoveAt((p_ID > -1) ? p_ID : m_WinningPatterns.Count - 1);
    public int f_GetActiveFreeGames() => FreeGames_Manager.m_Instance.m_ActiveFreeGames;
    
}
