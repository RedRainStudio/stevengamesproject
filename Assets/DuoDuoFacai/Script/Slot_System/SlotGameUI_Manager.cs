﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using MEC;
public class SlotGameUI_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static SlotGameUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====

    [Header("UI")]
    public TextMeshProUGUI m_MoneyText;
    public TextMeshProUGUI m_BigWinningMoneyText;
    public TextMeshProUGUI m_FreeGamesMoneyText;
    public TextMeshProUGUI m_SpinQuantity;
    public GameObject m_Infinite;
    public GameObject m_SpinHold;

    [Header("BottomUI")] 
    public TextMeshProUGUI m_WinningMoneyText;
    public TextMeshProUGUI m_FreeGamesText;
    public GameObject m_SpinButton;
    public Button m_MaxBetButton;
    public GameObject m_FreeGamesInfo;

    [Header("Max Bet Sprite")]
    public Sprite m_MaxBetOn;
    public Sprite m_MaxBetOff;

    [Header("Stop Sprite")]
    public Button m_StopButton;
    public Sprite m_StopButtonOff;
    public Sprite m_StopButtonOn;

    [Header("Min Plus Sprite")]
    public Button m_MinButton;
    public Button m_MaxButton;
    public Sprite m_MinSpriteOff;
    public Sprite m_MinSpriteOn;
    public Sprite m_MaxSpriteOn;
    public Sprite m_MaxSpriteOff;

    [Header("Information")]
    public Button m_InfoButton;
    public Sprite m_InfoOn;
    public Sprite m_InfoOff;

    [Header("PopUp")]
    public GameObject m_DimObject;

    [Header("BigWin")]
    public GameObject m_BigWinObject;
    public ParticleSystem m_BigWinParticle;
    public Sprite[] m_BigWinSprite;
    public Image m_BigWinImage;
    public GameObject m_BigWinCloseButton;

    [Header("JackPotGames")]
    public GameObject m_JackPotObject;
    public ParticleSystem m_JackPotParticle;
    public Sprite[] m_JackPotSprite;
    public Sprite[] m_JackPotCoinsSprite;
    public Sprite[] m_JackPotTextBorderSprite;
    public Image m_JackPotImage;
    public Image m_JackPotCoins;
    public Image m_JackPotTextBorder;
    public Image m_JackPotLight;
    public GameObject m_JackPotCloseButton;
    public Image m_PotLevelImage;
    public Sprite[] m_PotLevelSprite;
    public TextMeshProUGUI m_JackPotMoneyText1;
    public TextMeshProUGUI m_JackPotMoneyText2;

    [Header("FreeGames")]
    public GameObject m_FreeGamesStart;
    public GameObject m_FreeGamesTemporary;
    public GameObject m_FreeGamesDone;
    public TextMeshProUGUI m_OnFreeGamesDoneText;
    public Button m_FreeGamesDoneCloseButton;
    public Sprite m_NormalBackground;
    public Sprite m_FreeGamesBackground;
    public Sprite m_NormalSlotBackground;
    public Sprite m_FreeGamesSlotBackground;
    public Image m_Background;
    public Image m_SlotBackground;
    public TextMeshProUGUI m_MultiplierFiveDragons;
    public Image m_MultiplierImage;
    public List<Sprite> m_ListMultiplier;
    
    [Header("5OfAKind")]
    public GameObject m_FiveOfAKind;

    [Header("Settings")]
    public Image m_Mute;
    public Sprite m_MuteOn;
    public Sprite m_MuteOff;
    public GameObject m_Exit;
    public GameObject m_Toogle;
    public GameObject m_ExitPopUp;
    public GameObject m_ExitBackground;
    public Transform m_ExitTransform;
    public Transform m_MuteTransform;
    //===== PRIVATES =====
    ParticleSystem.EmissionModule m_Emission;
    private bool m_ActiveToogle;
    public AudioSource m_Audio;

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        if (Volume_Testing.m_Instance && Volume_Testing.m_Instance.m_IsMute)
        {
            m_Audio.mute = true;
            m_Mute.sprite = m_MuteOn;
            AudioManager_Manager.m_Instance.m_Mute = Volume_Testing.m_Instance.m_IsMute;
        }
    }

    void Update(){
    //    if (Input.GetKeyDown(KeyCode.Q)) {
    //        f_SetJackPot(e_LeagueType.GRAND);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.W)) {
    //        f_SetJackPot(e_LeagueType.MAJOR);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.E)) {
    //        f_SetJackPot(e_LeagueType.MINOR);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.R)) {
    //        f_SetJackPot(e_LeagueType.MINI);
    //    }
        
        f_UpdateText(e_TextType.MONEY, SlotGame_Manager.m_Instance.m_Player.m_Money.ToString("N0"));
        f_UpdateText(e_TextType.FREEGAMES, "FREE SPIN " + FreeGames_Manager.m_Instance.m_ActiveFreeGames.ToString() + "/" + FreeGames_Manager.m_Instance.m_TotalFreeGames);
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Mute()
    {
        if (m_Audio.mute)
        {
            m_Audio.mute = false;
            Volume_Testing.m_Instance.m_IsMute = false;
            m_Mute.sprite = m_MuteOff;
            AudioManager_Manager.m_Instance.m_Mute = false;
        }
        else
        {
            m_Audio.mute = true;
            Volume_Testing.m_Instance.m_IsMute = true;
            m_Mute.sprite = m_MuteOn;
            AudioManager_Manager.m_Instance.m_Mute = true;
        }
    }
    public void f_SetSpinButton(int p_Status) {
        if (p_Status == 0) {
            m_StopButton.enabled = true;
            m_StopButton.gameObject.SetActive(false);
            m_SpinButton.gameObject.SetActive(true);
            m_InfoButton.image.sprite = m_InfoOn;
            m_MinButton.image.sprite = m_MinSpriteOn;
            m_MaxButton.image.sprite = m_MaxSpriteOn;
            m_MaxBetButton.image.sprite = m_MaxBetOn;
            m_InfoButton.enabled = true;
            m_MinButton.enabled = true;
            m_MaxButton.enabled = true;
            m_MaxBetButton.enabled = true;

        }
        else if(p_Status == 1) {
            m_SpinButton.gameObject.SetActive(false);
            m_StopButton.gameObject.SetActive(true);

            m_StopButton.image.sprite = m_StopButtonOff;
            m_StopButton.enabled = false;
            m_InfoButton.image.sprite = m_InfoOff;
            m_MinButton.image.sprite = m_MinSpriteOff;
            m_MaxButton.image.sprite = m_MaxSpriteOff;
            m_MaxBetButton.image.sprite = m_MaxBetOff;
            m_InfoButton.enabled = false;
            m_MinButton.enabled = false;
            m_MaxButton.enabled = false;
            m_MaxBetButton.enabled = false;
        }
        else if(p_Status == 2) {
            m_StopButton.gameObject.SetActive(true);
            m_StopButton.image.sprite = m_StopButtonOn;
            m_SpinButton.gameObject.SetActive(false);
            m_StopButton.enabled = true;
            m_InfoButton.image.sprite = m_InfoOff;
            m_MinButton.image.sprite = m_MinSpriteOff;
            m_MaxButton.image.sprite = m_MaxSpriteOff;
            m_MaxBetButton.image.sprite = m_MaxBetOff;
            m_InfoButton.enabled = false;
            m_MinButton.enabled = false;
            m_MaxButton.enabled = false;
            m_MaxBetButton.enabled = false;

        }
    }

    public void f_UpdateText(e_TextType p_Type, string p_Text) {
        switch (p_Type) {
            case e_TextType.MONEY: m_MoneyText.text = p_Text;break;
            case e_TextType.BIGWINMONEY: m_BigWinningMoneyText.text = p_Text; break;
            case e_TextType.FREEGAMESMONEY: m_FreeGamesMoneyText.text = p_Text; break;
            case e_TextType.WINNINGMONEY: m_WinningMoneyText.text = p_Text; break;
            case e_TextType.FREEGAMES: m_FreeGamesText.text = p_Text; break;
            
        } 
    }

    public void f_SetBigWin(double p_WinAmount, double p_BettingAmount) {
        AudioManager_Manager.m_Instance.ie_PlayDelayLoop(AudioManager_Manager.m_Instance.m_BigWin);
        Timing.RunCoroutine(AudioManager_Manager.m_Instance.ie_PlayDelayLoop(AudioManager_Manager.m_Instance.m_ParticleCoin));
        m_Emission = m_BigWinParticle.emission;
        if (p_WinAmount >= p_BettingAmount * 50) {
            m_BigWinImage.sprite = m_BigWinSprite[2];
            m_Emission.rateOverTime = 100;
        } else if (p_WinAmount >= p_BettingAmount * 25) {
            m_BigWinImage.sprite = m_BigWinSprite[1];
            m_Emission.rateOverTime = 50;
        }
        else if(p_WinAmount >= p_BettingAmount * 10) { 
            m_BigWinImage.sprite = m_BigWinSprite[0];
            m_Emission.rateOverTime = 25;
        }
       
        m_BigWinObject.SetActive(true);
    }

    public void f_SetJackPot(e_LeagueType p_JackPotType) {
        m_Emission = m_JackPotParticle.emission;

        m_JackPotImage.sprite = m_JackPotSprite[((int)p_JackPotType) - 1];
        m_JackPotCoins.sprite = m_JackPotCoinsSprite[((int)p_JackPotType) - 1];
        m_JackPotTextBorder.sprite = m_JackPotTextBorderSprite[((int)p_JackPotType) - 1];
        if(p_JackPotType == e_LeagueType.GRAND) {
            m_JackPotLight.color = new Color(255/255f, 255/255f, 143/255f, 1f);
            m_Emission.rateOverTime = 75;
        }
        else if(p_JackPotType == e_LeagueType.MAJOR) {
            m_JackPotLight.color = new Color(255/255f, 136/255f, 253/255f, 0.5f);
            m_Emission.rateOverTime = 30;
        }
        else if (p_JackPotType == e_LeagueType.MINOR) {
            m_JackPotLight.color = new Color(23/255f, 192/255f, 255/255f, 0.5f);
            m_Emission.rateOverTime = 0;
        }
        else if (p_JackPotType == e_LeagueType.MINI) {
            m_JackPotLight.color = new Color(74/255f, 255/255f, 0/255f, 0.2f);
            m_Emission.rateOverTime = 0;
        }
       

        m_JackPotObject.SetActive(true);

    }

    public void f_SetFiveDragonJackPot(e_LeagueType p_JackPotType) {

        m_JackPotImage.sprite = m_JackPotSprite[((int)p_JackPotType)];
        m_JackPotObject.SetActive(true);
    }

    public void f_SetFreeGames(bool p_IsStart) {
        if (p_IsStart) {
            m_FreeGamesStart.SetActive(true);
        }
        else {
            
            m_FreeGamesDone.SetActive(true);
            if(m_OnFreeGamesDoneText)m_OnFreeGamesDoneText.text = "IN " + FreeGames_Manager.m_Instance.m_TotalFreeGames + " FREE SPINS";
        }
    }

    public void f_SetMaxBetButton(bool p_Active) {
        if (p_Active) {
            m_MaxBetButton.gameObject.SetActive(true);
            m_FreeGamesInfo.SetActive(false);
        }
        else {
            m_MaxBetButton.gameObject.SetActive(false); 
            m_FreeGamesInfo.SetActive(true);
        }
    }

    public void f_SetPotLevelImage(int p_PotLevel) {
        m_PotLevelImage.sprite = m_PotLevelSprite[p_PotLevel>=m_PotLevelSprite.Length?m_PotLevelSprite.Length-1:p_PotLevel];
    }

    public void f_SetBackground(bool p_FreeGames) {
        if (!p_FreeGames) {
            m_Background.sprite = m_NormalBackground;
            if(m_SlotBackground) m_SlotBackground.sprite = m_NormalSlotBackground;
        }
        else{ 
            m_Background.sprite = m_FreeGamesBackground;
            if (m_SlotBackground) m_SlotBackground.sprite = m_FreeGamesSlotBackground;
        }
    }

    public void f_ToogleSettings() {
        iTween.PunchScale(m_Toogle, iTween.Hash("amount", new Vector3(0.2f, 0.2f, 0.2f), "time", 0.5f));
        if (!m_ActiveToogle) {
            iTween.MoveTo(m_Exit, iTween.Hash("position", m_ExitTransform.position , "time", 0.3f, "easetype", iTween.EaseType.easeInOutQuad));
            iTween.MoveTo(m_Mute.gameObject, iTween.Hash("position", m_MuteTransform.position, "time", 0.3f, "easetype", iTween.EaseType.easeInOutQuad));
            m_ActiveToogle = true;
        }
        else {
            iTween.MoveTo(m_Exit, iTween.Hash("position", m_Toogle.transform.position, "time", 0.3f, "easetype", iTween.EaseType.easeInOutQuad));
            iTween.MoveTo(m_Mute.gameObject, iTween.Hash("position", m_Toogle.transform.position, "time", 0.3f, "easetype", iTween.EaseType.easeInOutQuad));
            m_ActiveToogle = false;
        }
    }

    
    public void f_TooglePopUp(bool p_Toogle)
    {
        if (p_Toogle)
        {
            m_ExitBackground.SetActive(true);
            iTween.ScaleTo(m_ExitPopUp, iTween.Hash("scale", new Vector3(1, 1, 1), "time", .5f, "easetype", iTween.EaseType.easeOutBounce));

        }
        else
        {
            m_ExitBackground.SetActive(false);
            m_ExitPopUp.transform.localScale = new Vector3(.75f, .75f, .75f);
        }
    }

    public bool f_IsJackPot => m_JackPotObject.activeSelf;

    public bool f_IsBigWin => m_BigWinObject.activeSelf;

    public bool f_IsStartedFreeGames => m_FreeGamesTemporary.activeSelf;
    public bool f_IsDoneFreeGames => m_FreeGamesDone.activeSelf;

}
