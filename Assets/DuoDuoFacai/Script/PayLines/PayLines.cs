﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

[Serializable]
public class PayLines {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public List<int> m_SlotsNumber = new List<int>();
    //public Color m_Color;
    public bool m_LinesEnabled;
    public bool m_UILinesEnabled;
    //===== PRIVATES =====
    int t_SlotNumber;
    //byte r;
    //byte g;
    //byte b;

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================
    public PayLines(/*string p_ColorString, */string[] p_SlotsNumber) {
        for(int i = 0; i < p_SlotsNumber.Length; i++) {
            int.TryParse(p_SlotsNumber[i], out t_SlotNumber);
            m_SlotsNumber.Add(t_SlotNumber);
        }
        
        //m_Color = f_GetColor(p_ColorString);
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
    //public Color f_GetColor(string p_Hex) {
    //    r = byte.Parse(p_Hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
    //    g = byte.Parse(p_Hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
    //    b = byte.Parse(p_Hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

    //    return new Color32(r, g, b, 255);
    //}

    
}
