﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;
public class LoadingScreen_GameObject : MonoBehaviour
{

    public GameObject m_Object;
    public Image m_LoadingBar;
    private void Awake() {
        m_Object.SetActive(true);
        m_LoadingBar.fillAmount = 0;
        Timing.RunCoroutine(co_Loading());
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator<float> co_Loading() {
        do {
            m_LoadingBar.fillAmount += 0.05f;
            yield return Timing.WaitForSeconds(Random.Range(0.05f, 0.2f));
        } while (m_LoadingBar.fillAmount < 1f);
        m_Object.SetActive(false);
    }

}
