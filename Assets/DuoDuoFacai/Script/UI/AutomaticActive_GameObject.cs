﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticActive_GameObject : MonoBehaviour
{
    public float m_Timer = 2f;
    float m_MaxTimer;
    // Start is called before the first frame update
    private void Awake() {
        m_MaxTimer = m_Timer;
    }
    void OnEnable()
    {
        m_Timer = m_MaxTimer;
    }

    // Update is called once per frame
    void Update()
    {
        m_Timer -= Time.deltaTime;
        if (m_Timer <= 0f) gameObject.SetActive(false);
    }
}
