﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Random = UnityEngine.Random;

    public class BettingUI_Manager : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static BettingUI_Manager m_Instance;
        //===== STRUCT =====
        //===== PUBLIC =====
        public TextMeshProUGUI m_TotalBetText;
        public TextMeshProUGUI m_TotalWinningText;
        public GameObject m_TotalWinningTextObject;
        public GameObject[] m_TotalWinningTextIdle;
        public GameObject[] m_TotalWinningTextSpinning;
        public GameObject m_TotalWinningTextLose;
        public TextMeshProUGUI m_JackpotWin;
        public List<DuoDuDuoFacai.JackpotCoin_Gameobject> m_Coins;
        public List<TextMeshProUGUI> m_JackPotInfo;
        public List<GameObject> m_PopUpGameObject;
        public GameObject m_MaxBetIndicator;
        public GameObject m_JackpotPanel;
        public Transform m_JackpotBowl;
        public bool m_DoneSpinning;
        public float t_WaitTimer;
        //===== PRIVATES =====
        int t_RandomIndex;
        private List<DuoDuDuoFacai.JackpotCoin_Gameobject> m_CoinPool = new List<DuoDuDuoFacai.JackpotCoin_Gameobject>();
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
        }

        private void Start() {
            Betting_Manager.m_Instance.m_OnChangeBetLevel += f_UpdateTotalBet;
            Betting_Manager.m_Instance.m_OnChangeBetLevel += f_UpdateUIBet;
            f_UpdateTotalBet(Betting_Manager.m_Instance.m_CurrentBetLevel);
            f_UpdateUIBet(Betting_Manager.m_Instance.m_CurrentBetLevel);
        }

        public void Update() {
            if (m_DoneSpinning) {
                t_WaitTimer += Time.deltaTime;
                if(t_WaitTimer > 5f) {
                    f_ResetTextWinning();
                    t_RandomIndex = Random.Range(0, 100);
                    if (t_RandomIndex < 50) m_TotalWinningTextIdle[0].SetActive(true);
                    else m_TotalWinningTextIdle[1].SetActive(true);
                    t_WaitTimer = 0f;
                }
            }
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_ResetTextWinning() {
            m_TotalWinningTextObject.SetActive(false);
            m_TotalWinningTextLose.SetActive(false);
            for(int i = 0; i < m_TotalWinningTextIdle.Length; i++) {
                m_TotalWinningTextIdle[i].SetActive(false);
            }
            for (int i = 0; i < m_TotalWinningTextSpinning.Length; i++) {
                m_TotalWinningTextSpinning[i].SetActive(false);
            }
        }
        public void f_UpdateTotalBet(BetLevel_Base p_BetLevel) {
            m_TotalBetText.text = p_BetLevel.m_CurrencyBetAmount.ToString("N0", CultureInfo.GetCultureInfo("en-US"));
        }

        public void f_UpdateUIBet(BetLevel_Base p_BetLevel) {
            if (Betting_Manager.m_Instance.m_IsMaxBet) m_MaxBetIndicator.SetActive(true);
            else m_MaxBetIndicator.SetActive(false);

            for (int i = 0; i < m_PopUpGameObject.Count; i++) {
                m_PopUpGameObject[i].SetActive(false);
            }
            if(Betting_Manager.m_Instance.m_GetBetLevelID < m_PopUpGameObject.Count) {
                m_PopUpGameObject[Betting_Manager.m_Instance.m_GetBetLevelID].SetActive(true);
            }
            
        }
        public void f_ResetCoins() {
            m_JackpotPanel.SetActive(true);
            Timing.RunCoroutine(ie_PoolCoin());
            //for (int i = 0; i < m_Coins.Count; i++) m_Coins[i].f_ResetCoin();
        }
        public void f_UpdateJackpotInfo(double p_BetAmount, JackpotLevel_Base p_Level, double p_GrandIncrease,double[] p_Winnings) {
            for (int i = 0; i < p_Winnings.Length; i++) {
                m_JackPotInfo[i].text = (p_Winnings[i] * p_BetAmount + (i < 3 ? 0 : p_GrandIncrease)).ToString("N0");
            }
        }

        public void f_UpdateJackpotInfo(double[] p_Winnings) {
            for (int i = 0; i < p_Winnings.Length; i++) {
                m_JackPotInfo[i].text = "" + p_Winnings[i];
            }
        }

        public void f_UpdateJackpotInfo(double p_BetAmount, float[] p_Winnings, double p_Jackpot) {
            for (int i = 0; i < p_Winnings.Length; i++) {
                m_JackPotInfo[i].text = (p_Winnings[i] * p_BetAmount).ToString("N0");
            }
        m_JackPotInfo[m_JackPotInfo.Count - 1].text = p_Jackpot.ToString("N0");
        }

    public void f_ActivateWinningAnimation(int p_WinningInt) {
            for (int i = 0; i < m_Coins.Count; i++) {
                if ((int)m_Coins[i].m_Type - 1 == p_WinningInt) {
                    m_Coins[i].f_ActivateAnimation();
                }
            }
        }

        public void f_ResetAllCoin() {
            for (int i = 0; i < m_Coins.Count; i++) {
                m_Coins[i].f_ResetCoin();
            }
        }

        IEnumerator<float> ie_PoolCoin() {
            m_CoinPool.Clear();
            m_CoinPool.AddRange(m_Coins);
            int t_Index = 0;
            int t_CointCount = m_CoinPool.Count;
            Debug.Log(t_CointCount);
            for (int i = 0; i <t_CointCount; i++) {
                t_Index = Random.Range(0, m_CoinPool.Count);
                m_CoinPool[t_Index].gameObject.SetActive(true);
                m_CoinPool.Remove(m_CoinPool[t_Index]);
                yield return Timing.WaitForSeconds(0.05f);
            }

            yield return Timing.WaitForSeconds(.2f);

            for (int i = 0; i < m_Coins.Count; i++) {
                m_Coins[i].m_GlowAnimatorCoin.SetActive(true);
            }

            DuoDuDuoFacai.JackPot_Manager.m_Instance.m_ClickJackpot = true;
        } 
    }