﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ObjectPooler;
namespace DuoDuDuoFacai {
    public class ParticlePool_Manager : PoolingManager_Manager<Particle_GameObject> {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static ParticlePool_Manager m_Instance;
        //===== STRUCT =====

        //===== PUBLIC =====
        public Particle_GameObject m_Prefab;
        public GameObject m_Parent;
        //===== PRIVATES =====
        Particle_GameObject m_Object;
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
        }

        private void Start() {
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_Spawn(Transform p_Spawn) {
            
            m_Object = f_SpawnObject(m_Prefab, p_Spawn);

            m_Object.f_Init(m_Parent.transform);
        }

    }
}