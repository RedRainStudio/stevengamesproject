﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Error_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Error_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_Text;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
        gameObject.SetActive(false);
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ErrorPopUp(string p_Message) {
        gameObject.SetActive(true);
        m_Text.text = p_Message;
    }
}
