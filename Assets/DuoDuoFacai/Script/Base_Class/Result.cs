﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
[Serializable]
public class Result
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public bool m_IS5OfAKind;
    public int m_Multiplier;
    public List<WinningObject> m_WinningResult3;
    public List<WinningObject> m_WinningResult4;
    public List<WinningObject> m_WinningResult5;
    public List<WinningObject> m_WinningResult;
    public List<int> m_MarkerIndex;
    //===== PRIVATES =====

    //=====================================================================
    //				    METHOD
    //=====================================================================
    public void f_Reset() {
        m_WinningResult3.Clear();

        m_WinningResult4.Clear();

        m_WinningResult5.Clear();

        m_WinningResult.Clear();
    }

    //=====================================================================
    //				    CHECK FOR DUPLICATE WIN
    //=====================================================================
    public void f_Check() {
        m_IS5OfAKind = false;
        for (int i = 0; i < m_WinningResult3.Count; i++) {
            for (int j = i + 1; j < m_WinningResult3.Count; j++) {
                if (m_WinningResult3[i].m_StartFromBack == m_WinningResult3[j].m_StartFromBack) {
                    if (m_WinningResult3[i].m_ID.Substring(0, m_WinningResult3[i].m_ID.Length - 4) == m_WinningResult3[j].m_ID.Substring(0, m_WinningResult3[j].m_ID.Length - 4)) {
                        m_WinningResult3.RemoveAt(j);
                        j--;
                    }
                }
            }

        }

        for (int i = 0; i < m_WinningResult4.Count; i++) {

            for (int j = i + 1; j < m_WinningResult4.Count; j++) {
                if (m_WinningResult4[i].m_StartFromBack == m_WinningResult4[j].m_StartFromBack) {
                    if (m_WinningResult4[i].m_ID.Substring(0, m_WinningResult4[i].m_ID.Length - 2) == m_WinningResult4[j].m_ID.Substring(0, m_WinningResult4[j].m_ID.Length - 2)) {
                        m_WinningResult4.RemoveAt(j);
                        j--;
                    }
                }
            }

            for (int k = 0; k < m_WinningResult3.Count; k++) {
                if (m_WinningResult4[i].m_StartFromBack == m_WinningResult3[k].m_StartFromBack) {
                    if (m_WinningResult3[k].m_ID.Substring(0, m_WinningResult3[k].m_ID.Length - 4) == m_WinningResult4[i].m_ID.Substring(0, m_WinningResult4[i].m_ID.Length - 4)) {
                        m_WinningResult3.RemoveAt(k);
                        k--;
                    }
                }
            }

        }

        for (int i = 0; i < m_WinningResult5.Count; i++) {

            for (int k = 0; k < m_WinningResult3.Count; k++) {
                if (m_WinningResult3[k].m_ID.Substring(0, m_WinningResult3[k].m_ID.Length - 4) == m_WinningResult5[i].m_ID.Substring(0, m_WinningResult5[i].m_ID.Length - 4)) {
                    m_WinningResult3.RemoveAt(k);
                    k--;
                }
            }

            for (int k = 0; k < m_WinningResult4.Count; k++) {
                if (m_WinningResult4[k].m_ID.Substring(0, m_WinningResult4[k].m_ID.Length - 2) == m_WinningResult5[i].m_ID.Substring(0, m_WinningResult5[i].m_ID.Length - 2)) {
                    m_WinningResult4.RemoveAt(k);
                    k--;
                }
            }

        }

        for (int i = 0; i < m_WinningResult3.Count; i++) {
            m_WinningResult.Add(m_WinningResult3[i]);
        }

        for (int i = 0; i < m_WinningResult4.Count; i++) {
            m_WinningResult.Add(m_WinningResult4[i]);
        }

        for (int i = 0; i < m_WinningResult5.Count; i++) {
            m_WinningResult.Add(m_WinningResult5[i]);
            m_IS5OfAKind = true;
        }



       f_Postgame(m_WinningResult.Count);
    }

    public void f_Postgame(int p_WinCount) {

        for (int i = 0; i < m_WinningResult.Count; i++) {
            //for (int j = 0; j < SlotGame_Manager.m_Instance.m_WinningPatterns.Count; j++) {
            //    if (SlotGame_Manager.m_Instance.f_IsMatch(SlotGame_Manager.m_Instance.m_WinningPatterns[j], m_WinningResult[i].m_ListItem)) {
            //        m_MarkerIndex.Add(j); //m_Marker[j].m_Object.SetActive(true);
            //    }
            //}
            if (m_WinningResult[i].m_StartFromBack) {
                for (int j = m_WinningResult[i].m_ListItem.Length - 1; j >= (m_WinningResult[i].m_ListItem.Length - m_WinningResult[i].m_TotalWinningReel); j--) {
                    SlotGame_Manager.m_Instance.m_Reels[j].m_Slots[m_WinningResult[i].m_Row[j]].f_WinningPattern();
                }
            }
            else {
                for (int j = 0; j < m_WinningResult[i].m_TotalWinningReel; j++) {
                    SlotGame_Manager.m_Instance.m_Reels[j].m_Slots[m_WinningResult[i].m_Row[j]].f_WinningPattern();
                }
            }
         

            if (m_IS5OfAKind && SlotGameUI_Manager.m_Instance.m_FiveOfAKind) SlotGameUI_Manager.m_Instance.m_FiveOfAKind.SetActive(true);
            else if (!m_IS5OfAKind && SlotGameUI_Manager.m_Instance.m_FiveOfAKind) SlotGameUI_Manager.m_Instance.m_FiveOfAKind.SetActive(false);
        }

        Timing.RunCoroutine(SlotGame_Manager.m_Instance.f_PostGame(Betting_Manager.m_Instance.f_CalculatingResult(this)));
      
    }

    public void f_PlayWinningAnimations() {
        for (int i = 0; i < m_WinningResult.Count; i++) {
            if (m_WinningResult[i].m_StartFromBack) {
                for (int j = m_WinningResult[i].m_ListItem.Length - 1; j >= (m_WinningResult[i].m_ListItem.Length - m_WinningResult[i].m_TotalWinningReel); j--) {
                    SlotGame_Manager.m_Instance.m_Reels[j].m_Slots[m_WinningResult[i].m_Row[j]].f_PlayAnimation();
                }
            }
            else {
                for (int j = 0; j < m_WinningResult[i].m_TotalWinningReel; j++) {
                    SlotGame_Manager.m_Instance.m_Reels[j].m_Slots[m_WinningResult[i].m_Row[j]].f_PlayAnimation();
                }
            }
            
        }
    }
}
