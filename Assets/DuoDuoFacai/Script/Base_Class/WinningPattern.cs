﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

[Serializable]
public class WinningPattern {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public List<e_SlotType> m_WinningPattern;
    public e_SlotType m_PatternType;
    public int m_TotalWinningReel;
    public float m_Multiplier;
    public bool m_StartFromBack;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public WinningPattern() {
        m_WinningPattern = new List<e_SlotType>() {
            e_SlotType.RANDOM,
            e_SlotType.RANDOM,
            e_SlotType.RANDOM,
            e_SlotType.RANDOM,
            e_SlotType.RANDOM,
        };

    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
