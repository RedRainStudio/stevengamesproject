﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enumeration
{
    public enum e_SlotType
    {
        SCATTER,
        WILD,
        DRAGONSHIP,
        TURTLE,
        PHOENIX,
        PEANUT,
        JACK,
        ACE,
        QUEEN,
        KING,
        COIN,
        RANDOM,
        NINE,
        TEN,
        FISH,
        LION,
        ANGPAO,
        DEFECTWILD,
        DRUM,
        FIREWORK,
        GREENBOY,
        JADE,
        MANBLACKBEARD,
        MANWHITEBEARD,
        PINKGIRL,

    }

    public enum e_RollResult
    {

    }

    public enum e_LeagueType
    {
        NONE = 0,
        MINI = 1,
        MINOR = 2,
        MAJOR = 3,
        GRAND = 4
    }

    public enum e_ComboType
    {

    }

    public enum e_TextType
    {
        MONEY,
        WINNINGMONEY,
        BIGWINMONEY,
        FREEGAMESMONEY,
        JACKPOTMONEY,
        FREEGAMES,

    }
}
