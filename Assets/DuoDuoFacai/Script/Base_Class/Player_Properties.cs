﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class Player_Properties {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC ===== 
    //public int m_Money { get => _Money; set => _Money = value; }
    //===== PRIVATES =====
    public double m_Money;
    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public Player_Properties() {
    }

   

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
