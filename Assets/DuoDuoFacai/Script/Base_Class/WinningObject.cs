﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

[Serializable]
public class WinningObject {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====
    
    //===== PUBLIC =====
    public int[] m_ListItem;
    public int[] m_Row;
    public e_SlotType m_PatternType;
    public int m_MatchCount;
    public float m_WinningValue;
    public int m_TotalWinningReel;
    public string m_ID;
    public bool m_StartFromBack;
    //===== PRIVATES =====
    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================
    public WinningObject(float p_WinningValue, int[] p_ListItem, int[] p_Row, e_SlotType p_PatternType, int p_TotalWinningReel, string p_ID, bool p_StartFromBack) {
        m_ID = p_ID;
        m_PatternType = p_PatternType;
        m_TotalWinningReel = p_TotalWinningReel;
        f_SetItem(p_ListItem, p_Row);
        f_SetWinningValue(p_WinningValue);
        m_StartFromBack = p_StartFromBack;
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================

    public void f_SetWinningValue(float p_WinningValue) {
        m_WinningValue = p_WinningValue;
        m_MatchCount = 1;
    }

    public void f_SetItem(int[] p_ListItem, int[] p_Row) {
        m_ListItem = new int[p_ListItem.Length];
        m_Row = new int[p_Row.Length];
        Array.Copy(p_ListItem, m_ListItem , p_ListItem.Length);
        Array.Copy(p_Row, m_Row, p_Row.Length);
    }

    

}
