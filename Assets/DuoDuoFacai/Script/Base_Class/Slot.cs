﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

[Serializable]
public class Slot {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====


    //===== PUBLIC =====
    public Symbol_Gameobject m_Object;
    public e_SlotType m_Type;
    public bool m_FrequencyPerBlock;
    public int[] m_Frequency;

    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================
    public Slot() {
        m_Frequency = new int[15];
    }
    //=====================================================================
    //				    METHOD
    //=====================================================================
     
}
