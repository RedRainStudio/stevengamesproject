﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

[Serializable]
public class BetLevel_Base {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public double m_CurrencyBetAmount;
    public double m_RealBetAmount = 0;
    public double m_RealBet {
        get {
            if (m_RealBetAmount != 0) return m_RealBetAmount;
            else return m_CurrencyBetAmount;
        }
        set {
            m_RealBetAmount = value;
        }
    }
    public e_LeagueType m_LeagueType;
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================

    public BetLevel_Base() {

    }

    //=====================================================================
    //				    METHOD
    //=====================================================================

}
