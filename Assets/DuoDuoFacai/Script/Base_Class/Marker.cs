﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class Marker {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_Text;
    public GameObject m_Object;
    
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\


    //=====================================================================
    //				    METHOD
    //=====================================================================
    public void f_SetText(int p_Amount) {
        m_Text.text = "";
    }
}
