﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;
using Enumeration;
[CustomEditor(typeof(SlotGame_Manager))]
public class SlotGame_Editor : Editor{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int tab;
    public int page;
    public int winpage;
    //===== PRIVATES =====
    SlotGame_Manager m_Target;

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    public override void OnInspectorGUI() {
        m_Target = (SlotGame_Manager)target;

        tab = GUILayout.Toolbar(tab, new string[] { "Slots", "Payline", "Basic", "WinPattern", "Default" });
        if (tab == 0) {
            EditorGUILayout.Space();

            for (int i = 0; i < m_Target.m_Slots.Count; i++) {
                GUILayout.BeginVertical(GetBoxStyle());

                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();
                GUILayout.Label((m_Target.m_Slots[i].m_Object != null) ? m_Target.m_Slots[i].m_Object.name : "None", GetHeaderStyle());
                GUILayout.EndHorizontal();
                EditorGUILayout.Space();

                m_Target.m_Slots[i].m_Object = (Symbol_Gameobject)EditorGUILayout.ObjectField("Prefab", m_Target.m_Slots[i].m_Object, typeof(Symbol_Gameobject), false);
                m_Target.m_Slots[i].m_Type = (e_SlotType)EditorGUILayout.EnumPopup("Type", m_Target.m_Slots[i].m_Type);

                m_Target.m_Slots[i].m_FrequencyPerBlock = EditorGUILayout.ToggleLeft("Enable Frequency Per Block", m_Target.m_Slots[i].m_FrequencyPerBlock);
                GUILayout.BeginHorizontal();
                GUILayout.Label("Frequency ");
                if (!m_Target.m_Slots[i].m_FrequencyPerBlock) {
                    for (int j = 0; j < m_Target.m_Reels.Count * m_Target.m_RowCount; j++) {
                        if ((j != 0) && (!m_Target.m_Slots[i].m_FrequencyPerBlock)) {
                          m_Target.m_Slots[i].m_Frequency[j] = m_Target.m_Slots[i].m_Frequency[0];
                        }
                        else {
                          m_Target.m_Slots[i].m_Frequency[j] = EditorGUILayout.IntField(m_Target.m_Slots[i].m_Frequency[j], GUILayout.MaxWidth(30));
                        }
                    }
                }
                else {
                    for (int j = 0; j < m_Target.m_Reels.Count; j++) {
                        GUILayout.BeginVertical();
                        for (int k = 0; k < m_Target.m_RowCount; k++) {
                            GUILayout.BeginHorizontal();
                            m_Target.m_Slots[i].m_Frequency[(m_Target.m_RowCount * j) + k] = EditorGUILayout.IntField(m_Target.m_Slots[i].m_Frequency[(m_Target.m_RowCount * j) + k], GUILayout.MaxWidth(30));
                            GUILayout.EndHorizontal();
                        }
                        GUILayout.EndVertical();
                    }
                }


                GUILayout.EndHorizontal();


                GUILayout.EndVertical();
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+", GUILayout.MaxWidth(20))) m_Target.f_AddSlots();
            if (GUILayout.Button("-", GUILayout.MaxWidth(20))) m_Target.f_RemoveSlots();
            GUILayout.EndHorizontal();
        }
        else if (tab == 1) {
            if (GUILayout.Button("Generate PayLines")) {
                m_Target.f_ClearPayLines();
                m_Target.f_ReadCSVFile();
            }
            if (GUILayout.Button("Clear PayLines")) {
                m_Target.f_ClearPayLines();
            }

            GUILayout.Label("Page", GetHeaderStyle());
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("-")) page = (page > 1) ? page - 1 : 1;
            page = page < 1 ? 1 : page;
            page = page > (m_Target.m_PayLines.Count / 10) + 1 ? (m_Target.m_PayLines.Count / 10) + 1 : page;
            page = EditorGUILayout.IntField(page, new GUIStyle("HelpBox") { alignment = TextAnchor.MiddleCenter });
            if (GUILayout.Button("+")) page = (page < (m_Target.m_PayLines.Count / 10) + 1) ? page + 1 : ((m_Target.m_PayLines.Count/10) + 1);
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginVertical(GetBoxStyle());
            if(page > 0 && page < (m_Target.m_PayLines.Count / 10) + 2) {
                for (int i = (page - 1) * 10; i < m_Target.m_PayLines.Count; i++) {
                    if (i < (page) * 10) {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("PayLines " + (i + 1).ToString("00"), GUILayout.Width(150));
                        for (int j = 0; j < m_Target.m_PayLines[i].m_SlotsNumber.Count; j++) {
                            GUILayout.Label(m_Target.m_PayLines[i].m_SlotsNumber[j].ToString(), GetBoxInsideStyle()/*, GUILayout.MaxWidth(30)*/);
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
           
            GUILayout.EndVertical();

        }
        else if (tab == 2) {
            GUILayout.BeginVertical(GetBoxStyle());

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("ReelCount ", new GUIStyle() { fontStyle = FontStyle.Bold }, GUILayout.Width(120));
            if (GUILayout.Button("-", GUILayout.MaxWidth(30))) m_Target.m_ReelCount--;
            GUILayout.Label(m_Target.m_ReelCount.ToString(), new GUIStyle("HelpBox") { alignment = TextAnchor.LowerCenter }, GUILayout.MaxHeight(18));
            if (GUILayout.Button("+", GUILayout.MaxWidth(30))) m_Target.m_ReelCount++;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("RowCount ", new GUIStyle() { fontStyle = FontStyle.Bold, }, GUILayout.Width(120));
            if (GUILayout.Button("-", GUILayout.MaxWidth(30))) m_Target.m_RowCount--;
            GUILayout.Label(m_Target.m_RowCount.ToString(), new GUIStyle("HelpBox") { alignment = TextAnchor.LowerCenter }, GUILayout.MaxHeight(18));
            if (GUILayout.Button("+", GUILayout.MaxWidth(30))) m_Target.m_RowCount++;
            EditorGUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }
        else if (tab == 3) {
            EditorGUILayout.Space();

            GUILayout.Label("Page", GetHeaderStyle());
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("-")) winpage = (winpage > 1) ? winpage - 1 : 1;
            winpage = winpage < 1 ? 1 : winpage;
            winpage = winpage > (m_Target.m_WinningPatterns.Count / 3) + 1 ? (m_Target.m_WinningPatterns.Count / 3) + 1 : winpage;
            winpage = EditorGUILayout.IntField(winpage, new GUIStyle("HelpBox") { alignment = TextAnchor.MiddleCenter });
            if (GUILayout.Button("+")) winpage = (winpage < (m_Target.m_WinningPatterns.Count / 3) + 1) ? winpage + 1 : ((m_Target.m_WinningPatterns.Count / 3) + 1);
            EditorGUILayout.EndHorizontal();

            if (winpage > 0 && winpage < (m_Target.m_WinningPatterns.Count / 3) + 2) {
                for (int i = (winpage - 1) * 3; i < m_Target.m_WinningPatterns.Count; i++) {
                    if (i < (winpage) * 3) {
                        GUILayout.BeginVertical(GetBoxStyle());

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Winning Pattern " + (i + 1).ToString("00"), GetHeaderStyle());
                        GUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        for (int j = 0; j < m_Target.m_WinningPatterns[i].m_WinningPattern.Count; j++) {
                            m_Target.m_WinningPatterns[i].m_WinningPattern[j] = (e_SlotType)EditorGUILayout.EnumPopup(m_Target.m_WinningPatterns[i].m_WinningPattern[j], GetBoxInsideStyle(m_Target.m_WinningPatterns[i].m_WinningPattern[j]));
                        }
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.Space();

                        GUILayout.Label("Pattern Type ", new GUIStyle() { alignment = TextAnchor.LowerCenter });

                        EditorGUILayout.BeginHorizontal();
                        m_Target.m_WinningPatterns[i].m_PatternType = (e_SlotType)EditorGUILayout.EnumPopup(m_Target.m_WinningPatterns[i].m_PatternType, GetBoxInsideStyle(m_Target.m_WinningPatterns[i].m_PatternType));
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Total Winning Pattern ", GUILayout.Width(150));
                        m_Target.m_WinningPatterns[i].m_TotalWinningReel = EditorGUILayout.IntField(m_Target.m_WinningPatterns[i].m_TotalWinningReel);
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("Winning Multiplier ", GUILayout.Width(150));
                        m_Target.m_WinningPatterns[i].m_Multiplier = EditorGUILayout.FloatField(m_Target.m_WinningPatterns[i].m_Multiplier);
                        EditorGUILayout.EndHorizontal();

                        GUILayout.EndVertical();
                    }

                }
            }

            EditorGUILayout.BeginHorizontal(); 
            if (GUILayout.Button("-")) m_Target.f_RemoveWinningPattern();
            if (GUILayout.Button("+")) m_Target.f_InsertWinningPattern();
            EditorGUILayout.EndHorizontal();
           
        }
        else { 
            base.OnInspectorGUI();
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public GUIStyle GetBoxStyle() {
        GUIStyle t_GuiStyle = new GUIStyle("HelpBox");
        t_GuiStyle.padding = new RectOffset(4, 4, 4, 4);
        return t_GuiStyle;
    }

    public GUIStyle GetBoxInsideStyle(e_SlotType p_Value = e_SlotType.RANDOM) {
        GUIStyle t_GuiStyle = new GUIStyle("HelpBox");
        Texture2D t_Texture = new Texture2D(10, 10);

        ///if(p_Value != e_SlotType.RANDOM) {
        for (int i = 0; i < t_Texture.width; i++) {
            for (int j = 0; j < t_Texture.height; j++) {

                //switch (p_Value) {
                //    case e_SlotType.PHOENIX: t_Texture.SetPixel(i, j, new Color(255 / 255f, 165 / 255f, 0 / 255f)); break;
                //    case e_SlotType.DRAGONSHIP: t_Texture.SetPixel(i, j, new Color(255 / 255f, 0 / 255f, 0 / 255f)); break;
                //    case e_SlotType.TURTLE: t_Texture.SetPixel(i, j, new Color(0 / 255f, 255 / 255f, 0 / 255f)); break;
                //    case e_SlotType.SCATTER: t_Texture.SetPixel(i, j, new Color(255 / 255f, 192 / 255f, 203 / 255f)); break;
                //    case e_SlotType.WILD: t_Texture.SetPixel(i, j, new Color(0 / 255f, 0 / 255f, 0 / 255f)); break;
                //    case e_SlotType.PEANUT: t_Texture.SetPixel(i, j, Color.cyan); break;
                //    case e_SlotType.ACE: t_Texture.SetPixel(i, j, Color.blue); break;
                //    case e_SlotType.KING: t_Texture.SetPixel(i, j, Color.magenta); break;
                //    case e_SlotType.QUEEN: t_Texture.SetPixel(i, j, Color.yellow); break;
                //    case e_SlotType.JACK: t_Texture.SetPixel(i, j, Color.grey); break;
                //    case e_SlotType.NINE: t_Texture.SetPixel(i, j, Color.white); break;
                //    case e_SlotType.TEN: t_Texture.SetPixel(i, j, new Color(255 / 255f, 110 / 255f, 203 / 255f)); break;
                //    case e_SlotType.COIN: t_Texture.SetPixel(i, j, Color.clear); break;
                //}

                if (i < 2f || i > t_Texture.width - 1 - 2f) t_Texture.SetPixel(i, j, new Color(0 / 255f, 0 / 255f, 0 / 255f));
                else if (j < 2f || j > t_Texture.height - 1 - 2f) t_Texture.SetPixel(i, j, new Color(0 / 255f, 0 / 255f, 0 / 255f));
            }
        }
        t_Texture.Apply();

        t_GuiStyle.normal.background = t_Texture;
        //}


        t_GuiStyle.padding = new RectOffset(4, 4, 4, 4);
        t_GuiStyle.alignment = TextAnchor.MiddleCenter;
        
       
        return t_GuiStyle;
    }


    public GUIStyle GetHeaderStyle() {
        GUIStyle t_GuiStyle = new GUIStyle();
        t_GuiStyle.alignment = TextAnchor.UpperCenter;
        t_GuiStyle.fontSize = 18;
        t_GuiStyle.fontStyle = FontStyle.Bold;
        return t_GuiStyle;
    }
}
