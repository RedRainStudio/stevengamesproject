﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

namespace DuoDuDuoFacai {
    public class Particle_GameObject : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        //===== STRUCT =====
        //===== PUBLIC =====
        public Transform m_Target;

        public RectTransform m_Rect;

        public float m_ArcHeight;
        Vector2 SpawnPos;
        bool t_Start = false;
        Transform m_realparent;
        //===== PRIVATES =====
        float m_Progress;
        float m_StepScale;
        Vector2 m_CurrentVelocity;
        Vector3 m_StartPosition;
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        private void Awake()
        { 
        }

        private void Update() {
            if (t_Start) {
                transform.position = Vector2.MoveTowards(transform.position, m_Target.position, 0.3f);
                if (Vector2.Distance(m_Rect.position, m_Target.position) < .1f) {
                    t_Start = false;
                    transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                    m_Rect.SetParent(ParticlePool_Manager.m_Instance.m_Parent.transform, true);
                    transform.localPosition = Vector3.zero;
                    m_Target.GetComponent<Animator>().Play("Mangkok");
                    gameObject.SetActive(false);
                }
            }
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_Init(Transform p_RealParent) {
            Timing.RunCoroutine(ie_Init(p_RealParent));
        }

        IEnumerator<float> ie_Init(Transform p_RealParent) {
            m_Target = BettingUI_Manager.m_Instance.m_JackpotBowl;
            transform.localPosition = Vector3.zero;
            yield return Timing.WaitForSeconds(.25f);
            m_Rect.SetParent(p_RealParent, true);
            m_realparent = p_RealParent;
            m_StartPosition = transform.position;
            yield return Timing.WaitForSeconds(.25f);
            gameObject.SetActive(true);
            t_Start = true;
        }

    }

}

