﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
namespace DuoDuDuoFacai {
    public class DuoDuDuoFacaiSymbol_Gameobject : Symbol_Gameobject {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        //===== STRUCT =====
        //===== PUBLIC =====
        public Sprite m_NormalImage;
        public Sprite m_GoldenImage;
        public bool m_BannerImage = false;
        //===== PRIVATES =====

        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        public override void Start() {
            if (m_SymbolType == e_SlotType.DRAGONSHIP || m_SymbolType == e_SlotType.PEANUT || m_SymbolType == e_SlotType.PHOENIX || m_SymbolType == e_SlotType.TURTLE) {
                if (m_BannerImage) Betting_Manager.m_Instance.m_OnChangeBetLevel += f_ChangeToGold;
                else Betting_Manager.m_Instance.m_OnSpinButton += f_ChangeToGold;
            }
        }

        public override void OnEnable() {
            if (!m_BannerImage) f_ChangeToGold(Betting_Manager.m_Instance.m_CurrentBetLevel);
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_ChangeToGold(BetLevel_Base p_BetLevel) {
            if (f_CheckGold(p_BetLevel)) m_Image.sprite = m_GoldenImage;
            else m_Image.sprite = m_NormalImage;
        }

        private bool f_CheckGold(BetLevel_Base p_BetLevel) {
            if (m_SymbolType == e_SlotType.PEANUT && (int)p_BetLevel.m_LeagueType > 0) return true;
            else if (m_SymbolType == e_SlotType.TURTLE && (int)p_BetLevel.m_LeagueType > 1) return true;
            else if (m_SymbolType == e_SlotType.DRAGONSHIP && (int)p_BetLevel.m_LeagueType > 2) return true;
            else if (m_SymbolType == e_SlotType.PHOENIX && (int)p_BetLevel.m_LeagueType > 3) return true;
            else return false;
        }
    }
}