﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

public class Symbol_Gameobject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public ReelSlot_GameObject m_ReelParent;
    public Animator m_Animation;
    public string m_AnimationName;
    public Image m_Image;
    public e_SlotType m_SymbolType;
    public GameObject m_Highlight;
    //===== PRIVATES =====
    protected int m_Index;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    public void Awake() {
        m_Animation = GetComponent<Animator>();
    }

    public virtual void Start() {
       
    }

    public virtual void OnEnable() {

    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public virtual void f_Initialize(ReelSlot_GameObject p_ReelParent, int p_Index) {
        m_ReelParent = p_ReelParent;
        m_Index = p_Index;
    }
    public virtual void f_OnSpin() {
        m_Highlight.SetActive(false);
    }

    public virtual void f_WinningPattern() {
        m_Highlight.SetActive(true);
        f_PlayAnimation();
    }

    public void f_AnimationDone() {
        SlotGame_Manager.m_Instance.m_SymbolAnimationDone = true;
    }

    //scatter
    public void f_AnimationScatterDOne() {
        SlotGame_Manager.m_Instance.m_ScatterAnimationDone = true;
    }

    public virtual void f_PlayAnimation() {
        m_Animation.Play(m_AnimationName);
    }
}
