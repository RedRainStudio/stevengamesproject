﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

namespace DuoDuDuoFacai {
    public class JackpotCoin_Gameobject : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        //===== STRUCT =====

        //===== PUBLIC =====
        public List<Sprite> m_GlowColor;
        public Animator m_Anim;
        public e_LeagueType m_Type;
        public List<JackpotCoinContainer_Gameobject> m_JackpotContainer;
        public Image m_FirstGlow;
        public Image m_SecondGlow;
        public GameObject m_GlowAnimatorCoin;
        //===== PRIVATES =====
        
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        public void Start() {
            //m_Button = GetComponent<Button>();
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_ResetCoin() {
            for (int i = 0; i < m_JackpotContainer.Count;i ++) {
                m_JackpotContainer[i].gameObject.SetActive(false);
                m_JackpotContainer[i].f_Deactivate();
            }
        }

        public void f_OpenCoin() {
            if (JackPot_Manager.m_Instance.m_ClickJackpot) {
                //m_Image.sprite = m_Sprites[JackPot_Manager.m_Instance.m_Sequence.Peek()];
                m_Type = (e_LeagueType) JackPot_Manager.m_Instance.m_Sequence.Peek() + 1;
                m_FirstGlow.sprite = m_GlowColor[(int)m_Type - 1];
                m_SecondGlow.sprite = m_GlowColor[(int)m_Type - 1];
                m_JackpotContainer[(int)m_Type - 1].gameObject.SetActive(true);
                JackPot_Manager.m_Instance.m_ClickJackpot = false;
                m_Anim.Play("Opening");
            }
        }

        public void f_DoneOpening (){
            JackPot_Manager.m_Instance.f_CheckSequence();
            m_JackpotContainer[(int)m_Type - 1].f_ActivateTextGlossy();
        }

        public void f_ActivateAnimation() {
            m_JackpotContainer[(int)m_Type - 1].f_Activate();
        }
    }
}