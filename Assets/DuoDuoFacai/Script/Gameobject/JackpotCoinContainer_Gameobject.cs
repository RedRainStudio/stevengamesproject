﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace DuoDuDuoFacai {
    public class JackpotCoinContainer_Gameobject : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        //===== STRUCT =====

        //===== PUBLIC =====
        public Animator m_AnimBody;
        public Animator m_Glow;
        public Animator m_GlossyText;
        //===== PRIVATES =====

        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Start() {

        }

        void Update() {

        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_Activate() {
            m_AnimBody.enabled = true;
            m_Glow.enabled = true;
        }

        public void f_ActivateTextGlossy() {
            m_GlossyText.gameObject.SetActive(true);
        }

        public void f_Deactivate() {
            m_AnimBody.enabled = false;
            m_Glow.enabled = false;
            m_GlossyText.gameObject.SetActive(false);

        }
    }
}