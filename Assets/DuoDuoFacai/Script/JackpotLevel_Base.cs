﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class JackpotLevel_Base {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public float[] m_Chances = new float[4];
    public string[] m_LeagueName = new string[4] { "Mini", "Minor", "Major", "Grand" };
    //===== PRIVATES =====

    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================\

    public JackpotLevel_Base() {
    }

    //=====================================================================
    //				    METHOD
    //=====================================================================
}
