﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using MEC;
using Random = UnityEngine.Random;

namespace DuoDuDuoFacai {
    public class JackPot_Manager : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static JackPot_Manager m_Instance;
        //===== STRUCT =====
        public class c_JackpotSequence {
            public int m_Sequence;
            public int m_MaxCall;
            public int m_CurrentCall;

            public c_JackpotSequence(int p_Sequence, int p_MaxCall) {
                m_CurrentCall = 0;
                m_Sequence = p_Sequence;
                m_MaxCall = p_MaxCall;
            }

            public void f_ResetCall(int p_MaxCall) {
                m_CurrentCall = 0;
                m_MaxCall = p_MaxCall;
            }

            public bool f_CheckCall() {
                m_CurrentCall++;
                if (m_CurrentCall == m_MaxCall) return true;
                else return false;
            }

        }
        //===== PUBLIC =====
        public Queue<int> m_Sequence = new Queue<int>();
        public List<JackpotLevel_Base> m_JackpotLevel;
        public int m_IncreaseJackpotValue;

        public double m_GrandPlusAmount = 0;
        public double[] m_WinningsMultiplier = new double[4] { 10, 25, 50, 200 };

        public float m_MinIncreaseJackpotProbabilty;
        public float m_MaxIncreaseJackpotProbabilty;

        public float m_MinIncreaseJackpotLevel;
        public float m_MaxIncreaseJackpotLevel;

        public float m_PotLevel;

        public float m_JackpotProbabilty;
        public float m_CapJackpotProbabilty;

        public bool m_ClickJackpot = false;

        public double m_GrandJackpotWinningValue;
        public double m_MajorJackpotWinningValue;
        public double m_MinorJackpotWinningValue;
        public double m_MiniJackpotWinningValue;
        //===== PRIVATES =====

        private List<c_JackpotSequence> m_JackpotSequenceList = new List<c_JackpotSequence>();
        private float m_DefaultJackpotProbabilty;

        List<c_JackpotSequence> t_JackpotSequenceList = new List<c_JackpotSequence>();
        float t_RandFloat;

        int t_JackpotLevelIndex = 0;
        int t_Rand;
        int t_WinningInt;
        int t_LeagueIndex;
        int t_JackpotSequence = 0;

        float t_IntervalLevel;
        float t_SumOfValue;
        double[] t_AllWinningValue = new double[4];

        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
            m_DefaultJackpotProbabilty = m_JackpotProbabilty;
            for (int i = 0; i < 4; i++) {
                m_JackpotSequenceList.Add(new c_JackpotSequence(i, 0));
            }
        }

        private void Start() {
            Betting_Manager.m_Instance.m_OnChangeBetLevelCurrentIndex += f_UpdateJackpotLevelID;
            //Betting_Manager.m_Instance.m_OnChangeBetLevel += f_UpdateJackpotInfo;
            t_IntervalLevel = 100f / SlotGameUI_Manager.m_Instance.m_PotLevelSprite.Length;
            //SlotGame_Manager.m_Instance.m_OnSpinDone.AddListener(f_OnSpinDone);
            //f_UpdateJackpotInfo(Betting_Manager.m_Instance.m_CurrentBetLevel);
        }

        private void Update() {
            SlotGameUI_Manager.m_Instance.f_SetPotLevelImage(Mathf.FloorToInt(m_PotLevel / t_IntervalLevel));
            f_UpdateJackpotInfo(m_GrandJackpotWinningValue, m_MajorJackpotWinningValue, m_MinorJackpotWinningValue, m_MiniJackpotWinningValue);
        }

        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_OnSpinDone() {
            for (int i = 0; i < SlotGame_Manager.m_Instance.m_WildCount; i++) {
                f_IncreaseJackpotLevel();
            }

            if (SlotGame_Manager.m_Instance.m_WildCount > 0) {
                if (m_PotLevel >= 100) f_CheckJackpot(Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType);
            }
        }

        public void f_UpdateJackpotInfo(BetLevel_Base p_BetLevel) {
            BettingUI_Manager.m_Instance.f_UpdateJackpotInfo(p_BetLevel.m_CurrencyBetAmount, m_JackpotLevel[t_JackpotLevelIndex], m_GrandPlusAmount,m_WinningsMultiplier);
        }

        public void f_UpdateJackpotInfo(double p_GrandValue, double p_MajorValue, double p_MinorValue, double p_MiniValue) {
            t_AllWinningValue[0] = p_MiniValue;
            t_AllWinningValue[1] = p_MinorValue;
            t_AllWinningValue[2] = p_MajorValue;
            t_AllWinningValue[3] = p_GrandValue;
            BettingUI_Manager.m_Instance.f_UpdateJackpotInfo(t_AllWinningValue);
        }

        public void f_UpdateJackpotLevelID(int p_BetLevelID) {
            t_JackpotLevelIndex = p_BetLevelID;
            if (t_JackpotLevelIndex >= m_JackpotLevel.Count) t_JackpotLevelIndex = m_JackpotLevel.Count - 1;
        }

        public void f_OpenMiniGame() {
            if (Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType > 0) {
                f_MakeSequence(Betting_Manager.m_Instance.m_CurrentBetLevel.m_LeagueType);
                BettingUI_Manager.m_Instance.f_ResetCoins();
            }
        }

        public void f_CheckJackpot(e_LeagueType p_LeagueType) {
            if (p_LeagueType > 0) {
                t_RandFloat = Random.Range(0f, 100f);
                if (t_RandFloat < m_JackpotProbabilty) {
                    Betting_Manager.m_Instance.m_Jackpot = true;
                    f_MakeSequence(p_LeagueType);
                    BettingUI_Manager.m_Instance.f_ResetCoins();
                    m_JackpotProbabilty = m_DefaultJackpotProbabilty;
                }
            }
        }

        public void f_IncreaseJackpotLevel() {
            m_GrandPlusAmount += m_IncreaseJackpotValue;

            if (m_PotLevel < 100f) {
                m_PotLevel += Random.Range(m_MinIncreaseJackpotLevel, m_MaxIncreaseJackpotLevel);
                if (m_PotLevel > 100f) m_PotLevel = 100f;
            }
            else {
                f_IncreaseJackpotProbabilty();
            }
        }

        public void f_IncreaseJackpotProbabilty() {
            if (m_JackpotProbabilty < m_CapJackpotProbabilty) {
                m_JackpotProbabilty += Random.Range(m_MinIncreaseJackpotProbabilty, m_MaxIncreaseJackpotProbabilty);
                if (m_JackpotProbabilty > m_CapJackpotProbabilty) m_JackpotProbabilty = m_CapJackpotProbabilty;
            }
        }

        public void f_MakeSequence(e_LeagueType p_LeagueType) {
            t_WinningInt = f_GetWinningCoin(p_LeagueType);

            t_LeagueIndex = ((int)p_LeagueType - 1) * 2;

            m_Sequence.Clear();
            t_JackpotSequenceList.Clear();

            for (int i = 0; i < 4; i++) {
                t_JackpotSequenceList.Add(m_JackpotSequenceList[i]);
                t_JackpotSequenceList[i].f_ResetCall(i == t_WinningInt ? 3 : 2);
            }

            for (int i = 0; i < 9; i++) {
                t_Rand = Random.Range(0, t_JackpotSequenceList.Count);
                m_Sequence.Enqueue(t_JackpotSequenceList[t_Rand].m_Sequence);
                if (t_JackpotSequenceList[t_Rand].f_CheckCall()) {
                    t_JackpotSequenceList.Remove(t_JackpotSequenceList[t_Rand]);
                }
            }

            t_JackpotSequence = 0;
        }

        public void f_CheckSequence() {
            if (m_Sequence.Peek() == t_WinningInt) {
                t_JackpotSequence++;
                if (t_JackpotSequence == 3) {
                    Timing.RunCoroutine(ie_ShowWinningAnimation());
                }
                else {
                    m_ClickJackpot = true;
                }
            }
            else {
                m_ClickJackpot = true;
            }
            m_Sequence.Dequeue();
        }

        public int f_GetWinningCoin(e_LeagueType p_LeagueType) {
            t_RandFloat = 1 + Random.Range(0f, 100f);
            t_SumOfValue = 0;

            for (int i = 0; i < (int)p_LeagueType; i++) {
                t_SumOfValue += m_JackpotLevel[t_JackpotLevelIndex].m_Chances[i];
                if (t_RandFloat <= t_SumOfValue) {
                    return i;
                }
            }
            return 0;
        }

        public IEnumerator<float> f_UpdateMoney(double p_Amount) {
            SlotGame_Manager.m_Instance.m_MoneyManager.m_JackpotWinAmount = p_Amount;
            double t_BigMoneyAnimation = 0;
            double t_MoneyPerSecond = p_Amount / 500;
            yield return Timing.WaitForOneFrame;
            AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_MegaWin, 1f);
            AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_ParticleCoin, 1f);
            while (SlotGameUI_Manager.m_Instance.f_IsJackPot) {
                while (t_BigMoneyAnimation != p_Amount) {
                    t_BigMoneyAnimation += t_MoneyPerSecond;
                    BettingUI_Manager.m_Instance.m_JackpotWin.text = t_BigMoneyAnimation.ToString("N0");
                    yield return Timing.WaitForOneFrame;
                }
                SlotGameUI_Manager.m_Instance.m_JackPotCloseButton.SetActive(true);
                yield return Timing.WaitForOneFrame;
            };

            Betting_Manager.m_Instance.m_Jackpot = false;

        }

        IEnumerator<float> ie_ShowWinningAnimation() {
            BettingUI_Manager.m_Instance.f_ActivateWinningAnimation(t_WinningInt);

            yield return Timing.WaitForSeconds(3);
            SlotGameUI_Manager.m_Instance.f_SetJackPot((e_LeagueType)t_WinningInt + 1);
            Timing.RunCoroutine(f_UpdateMoney(t_AllWinningValue[t_WinningInt]/*m_WinningsMultiplier[t_WinningInt] * Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount*/));
            if (t_WinningInt == 3) m_GrandPlusAmount = 0;
        }
    }
}