﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumeration;

public class SlotMoneyFiveDragon_Manager : SlotMoney_Manager
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====

    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
    }

    void Start() {

    }

    void Update() {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override IEnumerator<float> f_UpdateMoney(double p_Amount) {
        
        if (p_Amount <= 0) {
            AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Dud, 1f);
            yield return Timing.WaitForOneFrame;
        }
        else {
           
            if (FreeGames_Manager.m_Instance.m_ActiveFreeGames > 0) {
                Debug.Log(p_Amount - m_WinningMoney);
                if (p_Amount - m_WinningMoney > 0)
                {
                    AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
                }
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount - m_WinningMoney, p_Result => {
                    m_WinningMoney += p_Result;
                    SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                }, 100)));

                yield return Timing.WaitForSeconds(0.5f);
            }
            else {
                //HABIS FREE GAMES BERARTI
                if (m_WinningMoney > 0) {
                    if(p_Amount - m_WinningMoney > 0)
                    {
                        AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
                    }
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount - m_WinningMoney, p_Result => {
                        m_WinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                    }, 100)));

                    if (SlotGame_Manager.m_Instance.m_Result.m_IS5OfAKind) yield return Timing.WaitForSeconds(2f);
                    else yield return Timing.WaitForSeconds(0.5f);
                    SlotGameUI_Manager.m_Instance.m_FreeGamesDoneCloseButton.enabled = false;
                    //FREEGAMES
                    SlotGameUI_Manager.m_Instance.f_SetFreeGames(false);
                    AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_FreeGamesWin, 1f);
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        m_FreeWinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.FREEGAMESMONEY, m_FreeWinningMoney.ToString("N0"));
                    }, 100)));
                    SlotGameUI_Manager.m_Instance.m_FreeGamesDoneCloseButton.enabled = true;
                    do {
                        yield return Timing.WaitForOneFrame;
                    } while (!SlotGameUI_Manager.m_Instance.f_IsDoneFreeGames);

                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                    }, 100)));

                    m_FreeWinningMoney = 0;
                    SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(true);

                }
                else {
                    AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                        SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                        m_WinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                    }, 100)));

                    if (SlotGame_Manager.m_Instance.m_Result.m_IS5OfAKind) yield return Timing.WaitForSeconds(2f);
                    else yield return Timing.WaitForSeconds(0.5f);

                }

                m_WinningMoney = 0;
            }
        }
        AudioManager_Manager.m_Instance.f_StopDelayLoop();
    }

    public override IEnumerator<float> f_UpdateJackPot() {
        do {
            yield return Timing.WaitForOneFrame;
        } while (Betting_Manager.m_Instance.m_Jackpot);
        Debug.Log(m_JackpotWinAmount);
        if (m_JackpotWinAmount > 0) {
            t_CurrentMoneyAnimation = 0;
            t_MoneyPerSecond = m_JackpotWinAmount / 100;
            for(int i=0;i< 100; i++) {
                t_CurrentMoneyAnimation += t_MoneyPerSecond;
                SlotGame_Manager.m_Instance.m_Player.m_Money += t_MoneyPerSecond;

                yield return Timing.WaitForOneFrame;
            }
            //while (t_CurrentMoneyAnimation != m_JackpotWinAmount) {
               
            //};
        }

        m_JackpotWinAmount = 0;
    }
}
