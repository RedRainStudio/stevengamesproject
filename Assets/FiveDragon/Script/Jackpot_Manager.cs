﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using MEC;
using UnityEngine.EventSystems;

namespace FiveDragon {
    public class Jackpot_Manager : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static Jackpot_Manager m_Instance;
        //===== STRUCT =====
        //===== PUBLIC =====
        public List<JackpotLevel_Base> m_JackpotLevel;
        public float[] m_Multipliers;
        public double m_GrandValue = 80000000;
        public GameObject m_JackPotPopUp;
        public FiveDragonSymbol_Gameobject[] m_AngpaoObjects;
        public double m_GrandJackpotWinningValue;
        public double m_MajorJackpotWinningValue;
        public double m_MinorJackpotWinningValue;
        public double m_MiniJackpotWinningValue;
        //===== PRIVATES =====
        int t_JackpotWinningInt = 0;
        float t_SumOfValue =0;
        float t_RandFloat = 0;
        int m_AngpaoCount;
        double[] t_AllWinningValue = new double[4];
        FiveDragonSymbol_Gameobject m_AngpaoObject;

        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
        }

        void Start() {
            //Betting_Manager.m_Instance.m_OnChangeBetLevel += f_UpdateJackpotInfo;
            //f_UpdateJackpotInfo(Betting_Manager.m_Instance.m_CurrentBetLevel);
        }

        private void Update() {
            f_UpdateJackpotInfo(m_GrandJackpotWinningValue, m_MajorJackpotWinningValue, m_MinorJackpotWinningValue, m_MiniJackpotWinningValue);
        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_CheckJackpot() {
            m_AngpaoCount = 0;
            for(int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
                for (int j = 0; j < SlotGame_Manager.m_Instance.m_Reels[i].m_Slots.Length; j++) { 
                    if(SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].m_SymbolType == e_SlotType.ANGPAO) {
                        m_AngpaoObjects[m_AngpaoCount].transform.position = SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[j].transform.position;
                        m_AngpaoObjects[m_AngpaoCount].gameObject.SetActive(true);
                        m_AngpaoCount++;
                    }
                
                }
            }

            if (m_AngpaoCount >= 3) {
                Betting_Manager.m_Instance.m_Jackpot = true;
                m_JackPotPopUp.SetActive(true);
            }
        }

        public int f_GetJackpot(JackpotLevel_Base p_JackpotLevel) {
            t_SumOfValue = 0;
            t_RandFloat = UnityEngine.Random.Range(0, 100);
            Debug.Log(t_RandFloat);
            for (int i = 0; i < p_JackpotLevel.m_Chances.Length; i++) {
                t_SumOfValue += p_JackpotLevel.m_Chances[i];
                if (t_RandFloat <= t_SumOfValue) {
                    return i;
                }
            }
            return 0;
        }

        public void f_ChooseJackpot(int p_Index) {
            m_AngpaoObject = m_AngpaoObjects[p_Index];
            t_JackpotWinningInt = f_GetJackpot(m_JackpotLevel[m_AngpaoCount - 3]);

            Debug.Log(t_JackpotWinningInt);
            Timing.RunCoroutine(ie_ShowWinningAnimation());
        }

        public IEnumerator<float> f_UpdateMoney(double p_Amount) {
            SlotGame_Manager.m_Instance.m_MoneyManager.m_JackpotWinAmount = p_Amount;
            double t_BigMoneyAnimation = 0;
            double t_MoneyPerSecond = p_Amount / 500;
            yield return Timing.WaitForOneFrame;
            AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_FreeGamesWin, 1f);
            while (SlotGameUI_Manager.m_Instance.f_IsJackPot) {
                SlotGameUI_Manager.m_Instance.m_JackPotCloseButton.GetComponent<Button>().enabled = false;
                while (t_BigMoneyAnimation != p_Amount) {
                    t_BigMoneyAnimation += t_MoneyPerSecond;
                    BettingUI_Manager.m_Instance.m_JackpotWin.text = t_BigMoneyAnimation.ToString("N0");
                    yield return Timing.WaitForOneFrame;
                }
                SlotGameUI_Manager.m_Instance.m_JackPotCloseButton.GetComponent<Button>().enabled = true;
                yield return Timing.WaitForOneFrame;
            };

            Betting_Manager.m_Instance.m_Jackpot = false;
        }

        IEnumerator<float> ie_ShowWinningAnimation() {
            m_AngpaoObject.f_ChangeImage(t_JackpotWinningInt);
            m_AngpaoObject.m_Animation.Play(m_AngpaoObject.m_AnimationName);
            
            yield return Timing.WaitForSeconds(2);

            Debug.Log(t_JackpotWinningInt);
            SlotGameUI_Manager.m_Instance.f_SetFiveDragonJackPot((e_LeagueType)t_JackpotWinningInt);

            if (t_JackpotWinningInt < 3) {
                Timing.RunCoroutine(f_UpdateMoney(t_AllWinningValue[t_JackpotWinningInt])/*Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount * m_Multipliers[t_JackpotWinningInt]*/);
            }
            else {
                Timing.RunCoroutine(f_UpdateMoney(m_GrandJackpotWinningValue));
            }
        }

        public void f_ResetJackPot() {
            m_AngpaoObject.m_Image.gameObject.SetActive(false);
            m_JackPotPopUp.SetActive(false);
            for(int i = 0; i < m_AngpaoObjects.Length; i++) {
                m_AngpaoObjects[i].gameObject.SetActive(false);
            }
        }

        public void f_UpdateJackpotInfo(BetLevel_Base p_BetLevel) {
            BettingUI_Manager.m_Instance.f_UpdateJackpotInfo(Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount, m_Multipliers, m_GrandValue);
        }
        public void f_UpdateJackpotInfo(double p_GrandValue, double p_MajorValue, double p_MinorValue, double p_MiniValue) {
            t_AllWinningValue[0] = p_MiniValue;
            t_AllWinningValue[1] = p_MinorValue;
            t_AllWinningValue[2] = p_MajorValue;
            t_AllWinningValue[3] = p_GrandValue;
            BettingUI_Manager.m_Instance.f_UpdateJackpotInfo(t_AllWinningValue);
        }

    }
}