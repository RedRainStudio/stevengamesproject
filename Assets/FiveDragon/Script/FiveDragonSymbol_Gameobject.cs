﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
namespace FiveDragon {
    public class FiveDragonSymbol_Gameobject :  Symbol_Gameobject {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        //===== STRUCT =====
        //===== PUBLIC =====
        public Color[] m_Color;
        [Header("Angpao JackPot")]
        public Sprite[] m_AngpaoSprite;
        public bool m_IsDisplay;
        [Header("Five Dragon Wild ")]
        public List<Sprite> m_YellowDragon;
        public List<Sprite> m_BlueDragon;
        public List<Sprite> m_BlackDragon;
        public List<Sprite> m_RedDragon;
        public List<Sprite> m_GreenDragon;
        public List<Sprite> m_PurpleDragon;
        //===== PRIVATES =====
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        public override void Start() {
            //if (m_SymbolType == e_SlotType.WILD) {
            //    if (FreeGames_Manager.m_Instance.m_IsFreeGames) {
            //        f_ChangeColor();
            //    }
            //    ResultMultipler_Manager.m_Instance.m_OnPickDragon.AddListener(f_ChangeColor);
            //    SlotGame_Manager.m_Instance.m_OnFreeGamesDone.AddListener(f_ResetColor); 
            //}

            if(m_SymbolType == e_SlotType.ANGPAO) {
                transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            }

        }

        public override void OnEnable()
        {
            if (m_SymbolType == e_SlotType.ANGPAO && m_IsDisplay)
            {
                m_Animation.Play("roll_angpao");
            }
        }

        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_ChangeColor() {
           // m_Image.sprite =// m_ListFiveDragonWild[ResultMultipler_Manager.m_Instance.m_SelectedMultiplier];
           // m_Image.color = m_Color[ResultMultipler_Manager.m_Instance.m_SelectedMultiplier];
        }

        public void f_ResetColor() {
            m_Image.color = Color.white;
        }

        public void f_ChangeImage(int p_Index) {
            m_Image.sprite = m_AngpaoSprite[p_Index];
            m_Image.gameObject.SetActive(true);
        }

        public void f_ChangeImageWild(int p_Index) {
            if (!FreeGames_Manager.m_Instance.m_IsFreeGames) {
                m_Image.sprite = m_YellowDragon[p_Index];
            }
            else {
                if (ResultMultipler_Manager.m_Instance.m_SelectedMultiplier == 0) {
                    m_Image.sprite = m_BlackDragon[p_Index];
                }
                else if (ResultMultipler_Manager.m_Instance.m_SelectedMultiplier == 1) {
                    m_Image.sprite = m_PurpleDragon[p_Index];
                }
                else if (ResultMultipler_Manager.m_Instance.m_SelectedMultiplier == 2) {
                    m_Image.sprite = m_BlueDragon[p_Index];
                }
                else if (ResultMultipler_Manager.m_Instance.m_SelectedMultiplier == 3) {
                    m_Image.sprite = m_RedDragon[p_Index];
                }
                else {
                    m_Image.sprite = m_GreenDragon[p_Index];
                }
            }

        }
            
    }

}