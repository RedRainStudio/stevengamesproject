﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DragonAnimation_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_Fire;
    public Transform m_SpawnPosition;
    public int m_FreeGames;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_ActivateFire()
    {
        m_Fire.transform.position = m_SpawnPosition.position;
        m_Fire.GetComponent<FireBreath_GameObject>().m_FreeGames = m_FreeGames;
        m_Fire.SetActive(true);
        AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_DragonRoar, 1f);
    }
}
