﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Rendering;

public class CutOut_SpecialGameObject : Image
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====

    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override Material materialForRendering {
        get {
            Material t_Mats = new Material(base.materialForRendering);
            t_Mats.SetInt("_StencilComp", (int)CompareFunction.LessEqual);
            t_Mats.SetInt("_Stencil", 1);
            return t_Mats;
        }
    }
}