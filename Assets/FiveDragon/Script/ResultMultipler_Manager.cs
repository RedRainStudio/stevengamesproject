﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using Random = UnityEngine.Random;
namespace FiveDragon {
    public class ResultMultipler_Manager : MonoBehaviour {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static ResultMultipler_Manager m_Instance;
        //===== STRUCT =====
        [Serializable]
        public class c_ResultsMultiplier {
            public int[] m_Multiplier;
        }
        //===== PUBLIC =====
        public c_ResultsMultiplier[] m_ResultsMultiplier;
        public int m_SelectedMultiplier = 0;
        public int m_CurrentMultiplier;
        public Event m_OnPickDragon;
        //===== PRIVATES =====

        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
        }

        void Start() {
            //Betting_Manager.m_Instance.m_OnCalculateResult += f_CalculateFinalResult;
            Betting_Manager.m_Instance.m_OnCalculateFinalResult += f_CalculateFinalResult;
        }

        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_PickDragon(int p_SelectedDragon) {
            m_SelectedMultiplier = p_SelectedDragon;
            m_CurrentMultiplier = m_ResultsMultiplier[m_SelectedMultiplier].m_Multiplier[Random.Range(0, m_ResultsMultiplier[m_SelectedMultiplier].m_Multiplier.Length)];  //f_CalculateMultiplier();
            Debug.Log(m_CurrentMultiplier);
            switch (m_CurrentMultiplier) {
                case 2 : SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[0]; break;
                case 3 : SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[1]; break;
                case 5 : SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[2]; break;
                case 8 : SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[3]; break;
                case 10: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[4]; break;
                case 15: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[5]; break;
                case 30: SlotGameUI_Manager.m_Instance.m_MultiplierImage.sprite = SlotGameUI_Manager.m_Instance.m_ListMultiplier[6]; break;
            }
            
            //SlotGameUI_Manager.m_Instance.m_MultiplierFiveDragons.text = "x" + m_CurrentMultiplier;
            m_OnPickDragon?.Invoke();
        }

        //public void f_ChangeMultiplier(WinningObject p_Result) {
        //    if (p_Result.m_PatternType == e_SlotType.ANGPAO) {
        //        Betting_Manager.m_Instance.m_WinningMultiplier = 0;
        //        Jackpot_Manager.m_Instance.f_CheckJackpot();
        //    }
        //    else {
        //        Betting_Manager.m_Instance.m_WinningMultiplier = 1;
        //    }
        //}

        public int f_CalculateMultiplier() {
            if (FreeGames_Manager.m_Instance.m_IsFreeGames) {
                Debug.Log(m_SelectedMultiplier);
                return m_ResultsMultiplier[m_SelectedMultiplier].m_Multiplier[Random.Range(0, m_ResultsMultiplier[m_SelectedMultiplier].m_Multiplier.Length)];
            }
            else return 1; 
        }

        public void f_CalculateFinalResult() {
            f_TwoScatter();
            if (FreeGames_Manager.m_Instance.m_IsFreeGames) {
                Betting_Manager.m_Instance.m_TotalWinning = Betting_Manager.m_Instance.m_FreeWinning + ((Betting_Manager.m_Instance.m_TotalWinning - Betting_Manager.m_Instance.m_FreeWinning) * m_CurrentMultiplier);
            }
        }

        public void f_TwoScatter() {
            if (SlotGame_Manager.m_Instance.m_ScatterCount == 2) 
                Betting_Manager.m_Instance.m_TotalWinning = Betting_Manager.m_Instance.m_TotalWinning + (Betting_Manager.m_Instance.m_CurrentBetLevel.m_RealBetAmount * 2);
        }

    }
}