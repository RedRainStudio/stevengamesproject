﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DuoFuDuoCai_MenuJackpotCounter : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static DuoFuDuoCai_MenuJackpotCounter m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public double m_JackPotValue;
    public double m_IncreasingValuePerSecond;
    public TextMeshProUGUI m_TextJackPot;
    //===== PRIVATES =====
    double t_Amount;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        t_Amount = 0;
    }

    void FixedUpdate(){
        m_TextJackPot.text = t_Amount.ToString("N0");
        if (t_Amount < m_JackPotValue) {
            t_Amount += (m_IncreasingValuePerSecond / 50);
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    
}
