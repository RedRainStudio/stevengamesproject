﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Proyecto26;

public class RestClient_GameObject : MonoBehaviour{

    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static RestClient_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    [System.Serializable]
    public class c_DemoPosts
    {
        public string id;
        public string title;
    }

    [System.Serializable]
    public class c_DemoComments
    {
        public string id;
        public string body;
        public string postId;
    }

    [System.Serializable]
    public class c_DemoProfile
    {
        public string name;
    }

    [System.Serializable]
    public class c_DemoParent
    {
        public c_DemoPosts[] posts;
        public c_DemoComments[] comments;
        public c_DemoProfile profile;
    }
    //===== PRIVATES =====
    public c_DemoParent m_DemoGet;
    public c_DemoPosts m_DemoPost;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        f_RequestGet();
        f_RequestPost();
    }

    void Update(){
       
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_RequestGet() {
        try {
            RestClient.Get(new RequestHelper() {
                Uri = "https://my-json-server.typicode.com/typicode/demo/dbs",
                
            }).Then(res => {
                m_DemoGet = JsonUtility.FromJson<c_DemoParent>(res.Text);
            });
        }
        catch {
            Debug.Log("ERROR");
        }
       
    }

    public void f_RequestPost() {
        RestClient.Post(new RequestHelper() {
            Uri = "https://my-json-server.typicode.com/typicode/demo/posts",
            ContentType = "application/json",
            BodyString = @"{
                ""id"" : ""4"",
                ""title"" : ""Post 4""
            }",
            EnableDebug = true
        }).Then(res => {
            
            m_DemoPost = JsonUtility.FromJson<c_DemoPosts>(res.Text);
        });
    }
}
