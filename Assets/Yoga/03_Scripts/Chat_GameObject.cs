﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Chat_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Chat_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public RectTransform m_ContentRect;
    public RectTransform m_TextFrameRect;
    public RectTransform m_TextMeshProUGUIRect;
    public TextMeshProUGUI m_DetailsText;
    public TextMeshProUGUI m_NameText;
    public Image m_ProfilePictureFrame;
    public Image m_ProfilePictureDetails;
    public int m_MessageLength;
    public int m_MaxMessage;
    public int m_Counter;
    public int m_MaxMessageInOneLine;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake() {
        m_Instance = this;
    }

    void OnEnable() {
        
    }

    void Update() {
        
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Initialize() {
        //Max Message In 1 Line : 64
        //Max Total Message : 256
        for (int i = 0; i < m_MessageLength; i++) {
            if(m_DetailsText.text[i] == '\n' && m_Counter < m_MaxMessageInOneLine) {
                m_Counter = 0;
            }
            else {
                m_Counter++;
            }

            if(m_Counter >= m_MaxMessageInOneLine) {
                m_DetailsText.text = m_DetailsText.text.Insert(i, "\n");
                m_Counter = 0;
            }
        }
       
        m_TextFrameRect.sizeDelta = new Vector2(m_DetailsText.preferredWidth + 25f, m_DetailsText.preferredHeight + 10f);
        m_ContentRect.sizeDelta = new Vector2(m_ContentRect.sizeDelta.x, m_ContentRect.sizeDelta.y + m_DetailsText.preferredHeight);
    
    }


}
