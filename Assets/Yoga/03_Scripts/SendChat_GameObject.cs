﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SendChat_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static SendChat_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TMP_InputField m_InputField;
    public RectTransform m_Content;
    public ScrollRect m_ScrollRect;
    public Chat_GameObject m_Chat;
    //===== PRIVATES =====
    Vector2 t_Vector;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        if (m_ScrollRect.verticalScrollbar.size < 1) {
            m_Content.pivot = f_GetVector2(0, 0);
        }
        else {
            m_Content.pivot = f_GetVector2(0, 1);
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Send(TMP_InputField p_InputField = null) {
        if (p_InputField != null) m_InputField = p_InputField;
        Chat_GameObject t_Chat = Instantiate(m_Chat, m_Content.transform);
        t_Chat.m_DetailsText.text = m_InputField.text;
        t_Chat.m_MessageLength = m_InputField.text.Length;
        t_Chat.f_Initialize();
        m_InputField.text = "";
    }

    public Vector2 f_GetVector2(float x, float y) {
        t_Vector.x = x;
        t_Vector.y = y;

        return t_Vector;
    }

    public void f_Close() {
        m_InputField.text = "";
    }
}
