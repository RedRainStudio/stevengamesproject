﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenu_AdsPanel_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static MainMenu_AdsPanel_Manager m_Instance;
    //===== CLASS =====
    [System.Serializable]
    public class c_Positions {
        public Vector2 m_PreviousPosition;
        public Vector2 m_CurrentPosition;
        public Vector2 m_NextPosition;

        public GameObject m_Container;
    }

    [System.Serializable]
    public class c_Attributes {
        public List<Sprite> m_SpriteIndex;
        public float m_SlideSpeed;
        public float m_IdleTime;
        public int m_CurrentIndex;
    }
    //===== PUBLIC =====
    public c_Positions m_Positions;
    public c_Attributes m_Attributes;
    public List<GameObject> m_MenuPanels;
    //===== PRIVATES =====
    float m_CurrentTime;

    GameObject m_Previous;
    GameObject m_Current;
    GameObject m_Next;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        m_CurrentTime = m_Attributes.m_IdleTime;
        f_InitiateImage();
    }

    void Update(){
        f_SlidingAnimation();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_SlidingAnimation() {
        if(m_CurrentTime > 0) {
            m_CurrentTime -= Time.deltaTime;
        }

        if (m_CurrentTime <= 0) {


            m_Previous = m_Positions.m_Container.transform.GetChild(0).gameObject;
            m_Current = m_Positions.m_Container.transform.GetChild(1).gameObject;
            m_Next = m_Positions.m_Container.transform.GetChild(2).gameObject;

            m_Previous.transform.localPosition = m_Positions.m_NextPosition;
            m_Current.transform.localPosition = Vector2.Lerp(m_Current.transform.localPosition, m_Positions.m_PreviousPosition,m_Attributes.m_SlideSpeed);
            m_Next.transform.localPosition = Vector2.Lerp(m_Next.transform.localPosition, m_Positions.m_CurrentPosition, m_Attributes.m_SlideSpeed);

            if (f_IsInPosition()) {
                m_Current.transform.SetSiblingIndex(0);
                m_Next.transform.SetSiblingIndex(1);
                m_Previous.transform.SetSiblingIndex(2);
                m_CurrentTime = m_Attributes.m_IdleTime;
                f_NextImage();
            }
        }
    }

    public bool f_IsInPosition() {
        if (Vector2.Distance(m_Previous.transform.localPosition, m_Positions.m_NextPosition) <= .5f &&
            Vector2.Distance(m_Current.transform.localPosition, m_Positions.m_PreviousPosition) <= .5f &&
            Vector2.Distance(m_Next.transform.localPosition, m_Positions.m_CurrentPosition) <= .5f) {
            return true;
        }
        return false;
    }

    public void f_InitiateImage() {
        m_Previous = m_Positions.m_Container.transform.GetChild(0).gameObject;
        m_Current = m_Positions.m_Container.transform.GetChild(1).gameObject;
        m_Next = m_Positions.m_Container.transform.GetChild(2).gameObject;

        m_Previous.GetComponent<Image>().sprite = m_Attributes.m_SpriteIndex[m_Attributes.m_SpriteIndex.Count-1];
        m_Current.GetComponent<Image>().sprite = m_Attributes.m_SpriteIndex[0];
        m_Next.GetComponent<Image>().sprite = m_Attributes.m_SpriteIndex[1];

        m_Attributes.m_CurrentIndex = 1;
    }

    public void f_NextImage() {
        m_Attributes.m_CurrentIndex++;
        if (m_Attributes.m_CurrentIndex > m_Attributes.m_SpriteIndex.Count-1) {
            m_Attributes.m_CurrentIndex = 0;
        }
        m_Previous.GetComponent<Image>().sprite = m_Attributes.m_SpriteIndex[m_Attributes.m_CurrentIndex];
    }

    public void f_OpenPanel() {
        if(m_Attributes.m_CurrentIndex-1 < 0) m_MenuPanels[m_Attributes.m_SpriteIndex.Count - 1].SetActive(true);
        else m_MenuPanels[m_Attributes.m_CurrentIndex-1].SetActive(true);
    }
}
