﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TopUp_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_ID;
    public int m_Price;
    public int m_CoinAmount;
    public TextMeshProUGUI m_PriceText;
    public TextMeshProUGUI m_CoinAmountText;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        f_ShowPriceAndAmount();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ShowPriceAndAmount() {
        m_PriceText.text = "IDR " + m_Price + "K";
        m_CoinAmountText.text = "" + m_CoinAmount.ToString("N0");
    }

    public void f_AssignPrice(int p_Amount, int p_ID) {
        if(m_ID == p_ID) m_Price = p_Amount;
    }

    public void f_AssignCoinAmount(int p_Amount, int p_ID) {
        if (m_ID == p_ID) m_CoinAmount = p_Amount;
    }

    public void f_TopUpButton() {
        if (m_ID == 1) {
            f_TopUpButton1();
        }
        else if (m_ID == 2) {
            f_TopUpButton2();
        }
        else if (m_ID == 3) {
            f_TopUpButton3();
        }
        else if (m_ID == 4) {
            f_TopUpButton4();
        }
        else if(m_ID == 5) {
            f_TopUpButton5();
        }
    }

    public void f_TopUpButton1() {

    }

    public void f_TopUpButton2() {

    }

    public void f_TopUpButton3() {

    }

    public void f_TopUpButton4() {

    }

    public void f_TopUpButton5() {

    }
}
