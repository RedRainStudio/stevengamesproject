﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendInChat_GameObject : MonoBehaviour
{
    public PrivateChatManager_Manager m_Manager;
    public Image m_HighLight;
    public SendChat_GameObject m_ChatObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void f_OpenChat() {
        m_Manager.f_CloseAllLists();

        if (m_ChatObject != null) m_ChatObject.gameObject.SetActive(true);
        else {
            m_ChatObject = Instantiate(m_Manager.m_SendChatPrefab, m_Manager.m_SendChatParent);
        }
        m_Manager.m_ActiveChat = m_Manager.m_ListChats.IndexOf(this);
        m_HighLight.enabled = true;

    }
}
