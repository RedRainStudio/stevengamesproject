﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PrivateChatManager_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public TMP_InputField m_InputField;
    public SendChat_GameObject m_SendChatPrefab;
    public Transform m_SendChatParent;
    public List<SendChat_GameObject> m_ListChatsDetails;
    public List<FriendInChat_GameObject> m_ListChats;
    public int m_ActiveChat;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_CloseAllLists() {
        for (int i = 0; i < m_ListChatsDetails.Count; i++) {
            m_ListChatsDetails[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < m_ListChats.Count; i++) {
            m_ListChats[i].m_HighLight.enabled = false;
        }
    }

   public void f_Send() {
        m_ListChatsDetails[m_ActiveChat].f_Send(m_InputField);
    }
}
