﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class MainMenu_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static MainMenu_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_ExtendedMenu;
    public GameObject m_SoundOn;
    public GameObject m_SoundOff;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start(){
        Profile_GameObject.m_Instance.m_UserName.text = "PWI";
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ChangeScene(string p_Scene) {
        SceneManager.LoadScene(p_Scene);
    }

    public void f_ToogleExtendedMenu() {
        if (m_ExtendedMenu.activeSelf) m_ExtendedMenu.SetActive(false);
        else m_ExtendedMenu.SetActive(true);
    }

    public void f_ToogleMute() {
        if (Volume_Testing.m_Instance.m_IsMute) {
            m_SoundOff.SetActive(false);
            m_SoundOn.SetActive(true);
            Volume_Testing.m_Instance.m_IsMute = false;
        }
        else {
            m_SoundOff.SetActive(true);
            m_SoundOn.SetActive(false);
            Volume_Testing.m_Instance.m_IsMute = true;
        }
    }
}
