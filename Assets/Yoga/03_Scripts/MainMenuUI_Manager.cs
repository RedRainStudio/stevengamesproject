﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainMenuUI_Manager : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static MainMenuUI_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public TextMeshProUGUI m_Coin;
    public TextMeshProUGUI m_Gold;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    private void Awake() {
        m_Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
}
