﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volume_Testing : MonoBehaviour
{
    public static Volume_Testing m_Instance;
    public bool m_IsMute;
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
