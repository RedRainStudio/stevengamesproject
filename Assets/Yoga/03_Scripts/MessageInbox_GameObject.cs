﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessageInbox_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_MessageDetails;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){

    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Open() {
        for (int i = 0; i < InboxManager_Manager.m_Instance.m_ListMessageDetailsContainer.Count; i++) InboxManager_Manager.m_Instance.m_ListMessageDetailsContainer[i].SetActive(false);
        m_MessageDetails.SetActive(true);
    }
}
