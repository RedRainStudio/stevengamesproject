﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;

public class Profile_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Profile_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("GameObject")]
    public GameObject m_ProfilePopUp;
    public GameObject m_MatchHistoryPrefab;
    public List<GameObject> m_ListMatchHistory;
    
    [Header("Image")]
    public Image m_ProfilePictureImage;
    public Image m_GenderImage;

    [Header("Button")]
    public Button m_NextPage;
    public Button m_LeftPage;

    [Header("TextMeshPro")]
    public TextMeshProUGUI m_UserName;
    public TextMeshProUGUI m_Coins;
    public TextMeshProUGUI m_Heart;
    //===== PRIVATES =====
    int m_ActiveIndex;
    [Header("Parent")]
    [SerializeField] Transform m_MatchHistoryParent;
    
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    private void OnEnable() {
    }


    void Start(){
        Debug.Log("Start Profile");
        m_Coins.text = "1000";
        m_Heart.text = "2000";
        
    }

    void Update(){

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    GameObject f_GetObject() {
        return Instantiate(m_MatchHistoryPrefab, m_MatchHistoryParent.transform);
    }

    public void f_Spawn() {
        m_ListMatchHistory.Add(f_GetObject());
        m_ListMatchHistory[m_ListMatchHistory.Count - 1].SetActive(false);
        
    }

    public void f_Next() {
        if (m_ActiveIndex < m_ListMatchHistory.Count-1) {
            m_ListMatchHistory[m_ActiveIndex].SetActive(false);
            m_ActiveIndex++;
            m_ListMatchHistory[m_ActiveIndex].SetActive(true);
        }
    }

    public void f_Previous() {
        if(m_ActiveIndex > 0) {
            m_ListMatchHistory[m_ActiveIndex].SetActive(false);
            m_ActiveIndex--;
            m_ListMatchHistory[m_ActiveIndex].SetActive(true);
        }
        
    }


}
