﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VIP_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static VIP_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    [Header("GameObject")]
    public GameObject m_VIPPopUp;
    public GameObject[] m_VIPBorderPrefab;
    public List<GameObject> m_ListVIPPackage;

    [Header("Image")]
    public Image m_ProgressBar;

    [Header("TextMeshPro")]
    public TextMeshProUGUI m_Coins;
    public TextMeshProUGUI m_VIPStatus;
    //===== PRIVATES =====
    int m_ActiveIndex;
    [Header("Parent")]
    [SerializeField] Transform m_VIPBorderParent;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    GameObject f_GetObject(int p_Index)
    {
        return Instantiate(m_VIPBorderPrefab[p_Index], m_VIPBorderParent.transform);
    }

    public void f_Spawn(int p_Index)
    {
        m_ListVIPPackage.Add(f_GetObject(p_Index));
        m_ListVIPPackage[m_ListVIPPackage.Count - 1].SetActive(false);

    }

    public void f_Next()
    {
        if (m_ActiveIndex < m_ListVIPPackage.Count - 1)
        {
            m_ListVIPPackage[m_ActiveIndex].SetActive(false);
            m_ActiveIndex++;
            m_ListVIPPackage[m_ActiveIndex].SetActive(true);
        }
    }

    public void f_Previous()
    {
        if (m_ActiveIndex > 0)
        {
            m_ListVIPPackage[m_ActiveIndex].SetActive(false);
            m_ActiveIndex--;
            m_ListVIPPackage[m_ActiveIndex].SetActive(true);
        }

    }
}
