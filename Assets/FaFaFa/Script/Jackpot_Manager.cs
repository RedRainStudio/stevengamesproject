﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;

namespace FaFaFa
{
    public class Jackpot_Manager : MonoBehaviour
    {
        //=====================================================================
        //				      VARIABLES 
        //=====================================================================
        //===== SINGLETON =====
        public static Jackpot_Manager m_Instance;
        //===== STRUCT =====

        //===== PUBLIC =====
        public List<int> m_ListJackPot;
        public List<e_SlotType> m_ListType;
        //===== PRIVATES =====

        e_SlotType m_PatternType;
        //=====================================================================
        //				MONOBEHAVIOUR METHOD 
        //=====================================================================
        void Awake() {
            m_Instance = this;
        }

        void Start() {

        }

        void Update() {

        }
        //=====================================================================
        //				    OTHER METHOD
        //=====================================================================
        public void f_CheckJackPot() {
            if (SlotGame_Manager.m_Instance.m_Result.m_WinningResult5.Count == 9 &&
           SlotGame_Manager.m_Instance.m_Result.m_WinningResult3.Count == 0 &&
           SlotGame_Manager.m_Instance.m_Result.m_WinningResult4.Count == 0) {
                m_PatternType = SlotGame_Manager.m_Instance.m_Result.m_WinningResult[0].m_PatternType;
                if (f_IsJackPot()) {
                    Debug.Log(m_PatternType);
                    switch (m_PatternType) {
                        case e_SlotType.WILD:SlotGameUI_Manager.m_Instance.m_JackPotImage.sprite = SlotGameUI_Manager.m_Instance.m_JackPotSprite[0];break;
                        case e_SlotType.MANBLACKBEARD: SlotGameUI_Manager.m_Instance.m_JackPotImage.sprite = SlotGameUI_Manager.m_Instance.m_JackPotSprite[1]; break;
                        case e_SlotType.MANWHITEBEARD: SlotGameUI_Manager.m_Instance.m_JackPotImage.sprite = SlotGameUI_Manager.m_Instance.m_JackPotSprite[1]; break;
                        default: SlotGameUI_Manager.m_Instance.m_JackPotImage.sprite = SlotGameUI_Manager.m_Instance.m_JackPotSprite[2]; break;
                    }
                    SlotGame_Manager.m_Instance.m_MoneyManager.m_JackpotWinAmount = m_ListJackPot[m_ListType.IndexOf(m_PatternType)] * Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount;
                    Betting_Manager.m_Instance.m_Jackpot = true;
                }
                else {
                    Betting_Manager.m_Instance.m_Jackpot = false;
                }

            }

        }

        public bool f_IsJackPot() {
            for (int i = 0; i < SlotGame_Manager.m_Instance.m_Result.m_WinningResult.Count; i++) {
                if (SlotGame_Manager.m_Instance.m_Result.m_WinningResult[i].m_PatternType != m_PatternType) {
                    return false;
                }
            }
            return true;
        }
    }

}
