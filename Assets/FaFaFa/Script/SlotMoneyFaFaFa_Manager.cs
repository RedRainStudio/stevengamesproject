﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;
using Enumeration;
 
public class SlotMoneyFaFaFa_Manager : SlotMoney_Manager{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public CurrencyConverter m_Converter = new CurrencyConverter();
    public List<Marker> m_Marker;
    public Event m_JackPotEvent;
    public double m_JackPotEarnings;
    [Header("UI")]
    //===== PRIVATES =====
    int m_MatchingRow;
    int m_MarkerIndex;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    // FAFAFA GA ADA FREE SPIN WIN ADANYA CMANBIG WIN PAS LAGI FREEGAMES
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override IEnumerator<float> f_UpdateMoney(double p_Amount) {
        m_JackPotEvent?.Invoke();
        if (Betting_Manager.m_Instance.m_Jackpot) {
            SlotGame_Manager.m_Instance.m_Result.f_PlayWinningAnimations();
            m_WinningMoney = 0;
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(m_JackpotWinAmount, p_Result => {
                m_WinningMoney += p_Result;
                SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
            }, 100)));

            do {
                yield return Timing.WaitForOneFrame;
            } while (!SlotGame_Manager.m_Instance.m_SymbolAnimationDone);

            SlotGameUI_Manager.m_Instance.m_JackPotObject.SetActive(true);
            m_JackPotEarnings = 0;
            AudioManager_Manager.m_Instance.ie_PlayDelayLoop(AudioManager_Manager.m_Instance.m_BigWin);
            Timing.RunCoroutine(AudioManager_Manager.m_Instance.ie_PlayDelayLoop(AudioManager_Manager.m_Instance.m_ParticleCoin));
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(m_JackpotWinAmount, p_Result => {
                m_JackPotEarnings += p_Result;
                SlotGameUI_Manager.m_Instance.m_JackPotMoneyText1.text = m_JackPotEarnings.ToString("N0");
                SlotGameUI_Manager.m_Instance.m_JackPotMoneyText2.text = m_JackPotEarnings.ToString("N0");
            }, 500)));

            SlotGameUI_Manager.m_Instance.m_JackPotCloseButton.SetActive(true);
            SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(true);

            yield return Timing.WaitForOneFrame;

        }
        else {
            Debug.Log(p_Amount);
            if (p_Amount <= 0) {
                AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Dud, 1f);
                yield return Timing.WaitForOneFrame;
            }
            else {
                if (FreeGames_Manager.m_Instance.m_ActiveFreeGames > 0) {
                    yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateMoney()));
                }
                else {
                    //HABIS FREE GAMES BERARTI
                    if (m_WinningMoney > 0 || SlotGameUI_Manager.m_Instance.m_Background.sprite == SlotGameUI_Manager.m_Instance.m_FreeGamesBackground) {
                       
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateMoney()));
                        //FREEGAMES
                        SlotGameUI_Manager.m_Instance.f_SetFreeGames(false);
                        SlotGameUI_Manager.m_Instance.f_SetBigWin(m_WinningMoney, Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount);
                        m_FreeWinningMoney = 0;
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                            m_FreeWinningMoney += p_Result;
                            SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.FREEGAMESMONEY, m_FreeWinningMoney.ToString("N0"));
                        }, 500)));

                        SlotGameUI_Manager.m_Instance.m_BigWinCloseButton.SetActive(true);
                        do {
                            yield return Timing.WaitForOneFrame;
                        } while (!SlotGameUI_Manager.m_Instance.f_IsDoneFreeGames);

                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                            SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                        }, 100)));

                        SlotGameUI_Manager.m_Instance.f_SetMaxBetButton(true);
                    }
                    else {
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateMoney()));
                        
                        if (m_WinningMoney > Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount * 10) {
                            f_ResetBigWinningMoney();
                            SlotGameUI_Manager.m_Instance.f_SetBigWin(m_WinningMoney, Betting_Manager.m_Instance.m_CurrentBetLevel.m_CurrencyBetAmount);
                            yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(p_Amount, p_Result => {
                                m_BigWinningMoney += p_Result;
                                SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.BIGWINMONEY, m_BigWinningMoney.ToString("N0"));
                            }, 500)));
                            SlotGameUI_Manager.m_Instance.m_BigWinCloseButton.SetActive(true);
                        }
                        yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(m_WinningMoney, p_Result => {
                            SlotGame_Manager.m_Instance.m_Player.m_Money += p_Result;
                        }, 100)));
                    }

                    m_WinningMoney = 0;
                }
            }
        }
        Betting_Manager.m_Instance.m_Jackpot = false;
        AudioManager_Manager.m_Instance.f_StopDelayLoop();

    }

    public IEnumerator<float> f_UpdateMoney() {
        for(int i = 0; i < SlotGame_Manager.m_Instance.m_Result.m_WinningResult.Count; i++) {
            m_MarkerIndex = f_CheckRow(SlotGame_Manager.m_Instance.m_Result.m_WinningResult[i].m_Row);
            if (m_MarkerIndex > -1) {
                yield return Timing.WaitForSeconds(0.5f);
                m_Marker[m_MarkerIndex].m_Object.SetActive(true);
                m_Marker[m_MarkerIndex].m_Text.text = "" + m_Converter.GetStringCurrency(SlotGame_Manager.m_Instance.m_Result.m_WinningResult[i].m_WinningValue * Betting_Manager.m_Instance.m_CurrentBetLevel.m_RealBet);
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(f_UpdateText(
                    SlotGame_Manager.m_Instance.m_Result.m_WinningResult[i].m_WinningValue * Betting_Manager.m_Instance.m_CurrentBetLevel.m_RealBet, p_Result => {
                        m_WinningMoney += p_Result;
                        SlotGameUI_Manager.m_Instance.f_UpdateText(e_TextType.WINNINGMONEY, m_WinningMoney.ToString("N0"));
                    }, 1)));
                yield return Timing.WaitForSeconds(0.5f);
                m_Marker[m_MarkerIndex].m_Object.SetActive(false);
            }
        }

        SlotGame_Manager.m_Instance.m_Result.f_PlayWinningAnimations();
        AudioManager_Manager.m_Instance.f_PlayOneshot(AudioManager_Manager.m_Instance.m_Win, 1f);
        yield return Timing.WaitForSeconds(0.5f);
    }

    public int f_CheckRow(int[] p_Row) {
        for(int i = 0; i < SlotGame_Manager.m_Instance.m_PayLines.Count; i++) {
            m_MatchingRow = 0;
            for (int j = 0; j < SlotGame_Manager.m_Instance.m_PayLines[i].m_SlotsNumber.Count; j++) {
                if (SlotGame_Manager.m_Instance.m_PayLines[i].m_SlotsNumber[j] == p_Row[j]) {
                    m_MatchingRow++;
                }
            }
            if (m_MatchingRow == 5) return i;
        }

        return -1;
    }

   
}
