﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FAFAFASymbol_GameObject : Symbol_Gameobject{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    public GameObject m_Dimmer;
    public Vector3 m_DefaultScale;
    //===== PRIVATES =====
 
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_Initialize(ReelSlot_GameObject p_ReelParent, int p_Index) {
        base.f_Initialize(p_ReelParent, p_Index);
        if (m_DefaultScale == Vector3.zero) m_DefaultScale = Vector3.one;
        transform.localScale = m_DefaultScale;
    }
    public void f_RemoveOldSymbol() {
        for (int i = 1; i < m_ReelParent.m_Slots.Length - 1; i++) {
            m_ReelParent.f_Unspawn(m_ReelParent.m_Slots[i]);
        }
    }

    public void f_AddWild() {
        for (int i = 1; i < m_ReelParent.m_Slots.Length - 1; i++) {
            m_ReelParent.f_Spawn(ref m_ReelParent.m_Slots[i], m_ReelParent.m_FinalValues[0], i);
            //m_ReelParent.m_Slots[i].f_PlayAnimation()
        }

    }

    public void f_PlayWildAnimation() {
        m_ReelParent.m_ReelWildAnimation.SetActive(true);
    }

    public void f_StopAnimation() {
        m_ReelParent.m_ReelWildAnimation.SetActive(false);
        SlotGame_Manager.m_Instance.m_DoneFinalCheck = true;
        //m_Animation.Play("wild_gold");
    }

    public void f_ToogleHighLightScatter() {
        m_Dimmer.SetActive(false);
    }

    public void f_ToogleHighLight() {
        if(SlotGame_Manager.m_Instance.m_Result.m_WinningResult.Count > 0 && !SlotGame_Manager.m_Instance.m_IsSpinning && !m_Highlight.activeSelf) m_Dimmer.SetActive(true);
        else m_Dimmer.SetActive(false);
       
    }

    public override void f_WinningPattern() { 
        m_Highlight.SetActive(true);
        m_Dimmer.SetActive(false);
    }
}
