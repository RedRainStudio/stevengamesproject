﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Test_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Test_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public List<int> t_SlotResults = new List<int>();
    int t_SymbolIndex;
    public int[] t_Value;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_TestBigWin() {
        SlotGame_Manager.m_Instance.m_SlotResults = f_CheckResult();
        for (int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
            t_Value = new int[SlotGame_Manager.m_Instance.m_RowCount];

            for (int j = 0; j < SlotGame_Manager.m_Instance.m_RowCount; j++) {
                t_Value[j] = SlotGame_Manager.m_Instance.m_SlotResults[SlotGame_Manager.m_Instance.m_RowCount * i + j];
            }
            SlotGame_Manager.m_Instance.m_Reels[i].f_SetResult(t_Value);
        }
        SlotGame_Manager.m_Instance.f_Stop();
      
    }

    public List<int> f_CheckResult() {
        t_SlotResults.Clear();

        for (int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
            for (int j = 0; j < SlotGame_Manager.m_Instance.m_RowCount; j++) {
                if (j == 0) t_SymbolIndex = 3;
                else {
                    t_SymbolIndex = UnityEngine.Random.Range(0, 9);
                }
               
            
                t_SlotResults.Add(t_SymbolIndex);
            }

        }

        return t_SlotResults;
    }
}
