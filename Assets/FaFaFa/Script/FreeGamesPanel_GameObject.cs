﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FreeGamesPanel_GameObject : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static FreeGamesPanel_GameObject m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====

    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_Deactivate() {
        gameObject.SetActive(false);
        SlotGame_Manager.m_Instance.m_DoneFinalCheck = true;
    }

    public void f_SetFinal() {
        SlotGame_Manager.m_Instance.m_Reels[FreeGames_Manager.m_Instance.m_WildPanelInverse.IndexOf(gameObject)].f_SetFinalValues();
    }
}
