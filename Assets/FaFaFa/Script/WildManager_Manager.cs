﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumeration;
using Random = UnityEngine.Random;
public class WildManager_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static WildManager_Manager m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_WildIndex = -1;
    //===== PRIVATES =====
    int t_Hash;
    int t_WildCount;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
     //   Debug.Log(m_Converter.GetStringCurrency(750000));
    }

    void Update(){
        
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_CheckWild() {
        SlotGame_Manager.m_Instance.m_DoneFinalCheck = false;
        t_WildCount = 0;
        for (int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
            if (SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[0].m_SymbolType == e_SlotType.WILD) t_WildCount++;
        }

        if (t_WildCount >= 2) {
            for (int i = 0; i < SlotGame_Manager.m_Instance.m_Reels.Count; i++) {
                if (SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[0].m_SymbolType == e_SlotType.WILD && SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[1].m_SymbolType != e_SlotType.WILD && SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[2].m_SymbolType != e_SlotType.WILD && SlotGame_Manager.m_Instance.m_Reels[i].m_IsTripleWild) {
                    SlotGame_Manager.m_Instance.m_SlotResults[SlotGame_Manager.m_Instance.m_RowCount * i + 1] = SlotGame_Manager.m_Instance.m_SlotResults[SlotGame_Manager.m_Instance.m_RowCount * i];
                    SlotGame_Manager.m_Instance.m_SlotResults[SlotGame_Manager.m_Instance.m_RowCount * i + 2] = SlotGame_Manager.m_Instance.m_SlotResults[SlotGame_Manager.m_Instance.m_RowCount * i];
                    SlotGame_Manager.m_Instance.m_Reels[i].m_Slots[0].m_Animation.Play("call_wild_gold");
                }
            }
        }
        else {
            SlotGame_Manager.m_Instance.m_DoneFinalCheck = true;
        }

    }

}
