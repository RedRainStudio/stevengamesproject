﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable]
public class CurrencyConverter {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====

    //===== STRUCT =====

    //===== PUBLIC =====
    [Serializable]
    public class Currency
    {
        public double m_ScoreSize;
        public string m_PostFix;
    }

    public Currency[] m_Currencies;
    //===== PRIVATES =====
    double t_LastingNumber;
    string m_ScoreString;
    double t_Threshold;
    string t_PostFix;
    //=====================================================================
    //				    CONSTRUCTOR
    //=====================================================================


    //=====================================================================
    //				    METHOD
    //=====================================================================
    public string GetStringCurrency(double p_Amount) {
        m_ScoreString = "";
        t_LastingNumber = 0;
        for (int i = m_Currencies.Length; i > 0; i--) {
            double m_SizeLength = Math.Floor((p_Amount - t_LastingNumber) / m_Currencies[i - 1].m_ScoreSize);
            //if (t_Threshold > 0 && m_SizeLength > 0) {
                //m_SizeLength = m_SizeLength / m_Currencies[i - 1].m_ScoreSize;
            //}

            t_LastingNumber = t_LastingNumber + m_SizeLength * m_Currencies[i - 1].m_ScoreSize;
           // if (m_SizeLength > 0) {
            //    if (t_Threshold > 0) {
            //        //m_ScoreString = m_ScoreString + m_SizeLength.ToString("F2").Substring(1, m_SizeLength.ToString().Length - 2);

            //    }
            //    else {
            if(m_SizeLength > 0) m_ScoreString = m_ScoreString + m_SizeLength.ToString() + m_Currencies[i - 1].m_PostFix;
              //  }
            //}//


            //if (m_SizeLength > 0 && t_Threshold == 0) {
            //    t_Threshold = m_Currencies[i - 1].m_ScoreSize;
            //    t_PostFix = m_Currencies[i - 1].m_PostFix;
            //}

        }
        //m_ScoreString = m_ScoreString + t_PostFix;
        return m_ScoreString;
    }
}
